<?php

include_once ('functions.php');

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?=$settings['title']?>  Analytics | MATM</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>


    <?=custom_headers()?>

</head>
<body>

<?php sidebar();?>

<div class="main-panel">
    <?php nav("Support"); ?>

    <?php get_keywords() ?>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card" style="padding: 10px">
                        <h2>Got a problem?</h2>
                        <p><strong>Use the form bellow to get in touch with the MATM web team, and we'll get back to you ASAP</strong><span style="float: right"><strong>Or</strong> <a href="support-tickets.php"> view your open support tickets here</a></span></p>
                        <div class="row">
                            <div class="col-md-6">
                                <form action="http://support.matm.co.uk/log_ticket.php" method="POST" class="support-form">
                                    <div class="form-group">
                                        <label for="type">Support Type:</label>
                                        <select type="text" class="form-control" name="type" id="type">
                                            <option value="Website issue">Website issue</option>
                                            <option value="New Domain Registration Request">New Domain Registration Request</option>
                                            <option value="New Email Address Setup">New Email Address Setup</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="subject">Subject:</label>
                                        <input type="text" class="form-control" name="subject" id="subject">
                                    </div>
                                    <div class="form-group">
                                        <label for="message">Message:</label>
                                        <textarea class="form-control" name="message" id="message"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc">Contact Email Address:</label>
                                        <input type="email" class="form-control" id="cc" name="ccemail">
                                    </div>
                                    <input type="hidden" name="returnURL" value="<?=$siteurl?>/support-tickets.php">
                                    <input type="hidden" name="clientID" value="<?=CLIENT_ID?>">
                                    <input type="submit" class="btn btn-default" value="Submit">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<?php footer()?>

</div>
</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

</html>