<?php
include_once ('functions.php');
$settings = get_settings();


	if(isset($_GET['s'])){
        $_SESSION['s'] = $_GET['s'] ;
	}
	if(isset($_GET['e'])){
		$_SESSION['e'] = $_GET['e'] ;
	}


?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?=$settings['title']?> Analytics | MATM</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script>
        var functions = [];
        function registerFunction(Thefunction, id, containerID) {
            functions.push([Thefunction, id, containerID]);
        }
        var stats = [];
        function registerStat(Thefunction, id) {
            stats.push([Thefunction, id]);
        }
        var chartCount = 0,
        loaded = 0;
    </script>
    <?=custom_headers(); ?>

</head>
<body>

<?php sidebar();?>

    <div class="main-panel">
    <?php nav("Dashboard"); ?>
        <div class="content">
            <table width="" border="0" class="keystats">
            <?

            ?>
                <tr class="domain_stats_data">
                    <td id="page_sessions">
                        <img src="<?=$siteurl?>/assets/img/analytics-graph.png"/>
                        <p>Page sessions:
                            <br/>
                            <strong class="sessions saving">Loading<span>.</span><span>.</span><span>.</span></strong>
                        </p>
                        <script>$(document).load(registerStat("get_sessions", "sessions"))</script>
                    </td>
                    <td id="users">
                        <img src="<?=$siteurl?>/assets/img/one_user.png"/>
                        <p>Users:
                            <br/>
                            <strong class="users saving">Loading<span>.</span><span>.</span><span>.</span></strong>
                        </p>
                        <script>$(document).load(registerStat("get_users", "users"))</script>

                    </td>
                    <td id="new_unique_users">
                        <img src="<?=$siteurl?>/assets/img/user.png"/>
                        <p>New users:
                            <br/>
                            <strong class="new_users saving">Loading<span>.</span><span>.</span><span>.</span></strong>
                        </p>
                        <script>$(document).load(registerStat("get_new_users", "new_users"))</script>

                    </td>
                    <td id="pages_viewed">
                        <img src="<?=$siteurl?>/assets/img/visitor.png"/>
                        <p>Pages viewed:
                            <br/>
                            <strong class="page_views saving">Loading<span>.</span><span>.</span><span>.</span></strong>
                        </p>
                        <script>$(document).load(registerStat("get_page_visits", "page_views"))</script>
                    </td>
                    <td id="avg_pages_viewed">
                        <img src="<?=$siteurl?>/assets/img/visitor.png"/>
                        <p>Avg. pages viewed:
                            <br/>
                            <strong class="avg_pages_session saving">Loading<span>.</span><span>.</span><span>.</span></strong>
                        </p>
                        <script>$(document).load(registerStat("avg_pages_session", "avg_pages_session"))</script>
                    </td>
                    <!--  -->
                </tr>
                <tr class="domain_stats_data row_two">
                    <td id="avg_time_on_site" >
                        <img src="<?=$siteurl?>/assets/img/clock.png"/>
                        <p>Avg. time on site:
                            <br/>
                            <strong class="session_duration saving">Loading<span>.</span><span>.</span><span>.</span></strong><strong> min</strong>
                        </p>
                        <script>$(document).load(registerStat("get_session_duration", "session_duration"))</script>
                    </td>
                    <td id="mobile_users">
                        <img src="<?=$siteurl?>/assets/img/mobile.png"/>
                        <p>Users from Mobiles / Tablets:
                            <br/>
                            <strong class="mobile_users saving">Loading<span>.</span><span>.</span><span>.</span></strong><strong>%</strong>
                        </p>
                        <script>$(document).load(registerStat("get_mobile_sessions", "mobile_users"))</script>
                    </td>
                    <td id="pagespeed" >
                        <img src="<?=$siteurl?>/assets/img/speedometer.png"/>
                        <p>Page&nbsp;Speed:
                            <br/>
                            <strong class="pagespeed saving">Loading<span>.</span><span>.</span><span>.</span></strong><strong>%</strong>
                        </p>
                        <script>$(document).load(registerStat("get_page_speed", "pagespeed"))</script>
                    </td>
                    <td id="avg_bounce" >
                        <img src="<?=$siteurl?>/assets/img/bounce.png"/>
                        <p>Avg. bounce rate:
                            <br/>
                            <strong class="bounce_rate saving">Loading<span>.</span><span>.</span><span>.</span></strong><strong>%</strong>
                        </p>
                        <script>$(document).load(registerStat("get_avg_Bounces", "bounce_rate"))</script>
                    </td>
                    <td id="adwords_cost" >
                        <img src="<?=$siteurl?>/assets/img/cost.png"/>
                        <p>Adwords Cost:
                            <br/>
                            <strong>£</strong><strong class="ad_cost saving">Loading<span>.</span><span>.</span><span>.</span></strong>
                        </p>
                        <script>$(document).load(registerStat("get_adwords_cost", "ad_cost"))</script>
                    </td>
                </tr>
            </table>
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="card heading" style="padding: 10px;text-align: center;">
                        <h2>Welcome to the <?=$settings['title']?> Analytics Dashboard.</h2>
                        <p><strong>Key website stats, Adwords performance charts and Keywords used to find your website.</strong></p>
                        <p><strong>You can edit the date range using the bar at the top of the page</strong></p>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <?php
                    $counter = 0;
                    $count = 0;
                    $settings = get_settings();
                    $charts = json_decode($settings['charts'])->charts;
	                $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                    foreach ($charts as $chart){
                        if($counter == 0) {
                            ?>
                            <div class="row">
                            <?php
                        }
                        ?>
                            <div class="col-md-<?=$chart->col?>">
                                <div class="card <?=$f->format($count);?>">
                                    <div class="loading"></div>
                                    <?php
	                                    $functionName = $chart->function;
                                        if(function_exists($functionName)) {
                                            ?>
                                            <div class="table-switch">
                                                <a href="#" onclick="return switchTable(this, '<?=$f->format($count) ?>', '<?=$chart->id?>')">Table view</a>
                                            </div>
                                            <?php
                                            print '<div id="'. $chart->id . '" style="height: 477px;"></div>';
	                                        print "<script>chartCount++; $(document).load(registerFunction('" . $functionName . "', '" . $chart->id . "', '" . $f->format($count) . "'))</script>";
                                        } else {
                                          print '<div id="'. $chart->id . '" style="height: 477px;"><h2>ERROR: Function "' . $functionName . '" Doesn\'t Exist</h2></div>';
                                        }
                                    ?>
                                </div>
                            </div>
                        <?php
                            $counter += $chart->col;
                        if($counter == 12) {
                            ?>
                            </div>
                            <?php
                                $counter = 0;
                        }
	                    $count++;
                    }
                ?>
            </div>
        </div>


        <?php footer()?>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>


</html>
