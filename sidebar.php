<?php
	include_once ('database.php');
	include_once ('analyticsFunctions.php');
	$db = new database();
	$settings = get_settings();
$sidebar = '
<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

        <!--
    
            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag
    
        -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    <img src="assets/img/' . $settings['logo'] . '"><strong class="logo-img">MATM</strong><br>Analytics Dashboard
                </a>
            </div>

            <ul class="nav">
                <li class="">
                    <a href="dashboard.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="keywords.php">
                        <i class="pe-7s-user"></i>
                        <p>Keywords</p>
                    </a>
                </li>
                <li>
                    <a href="support.php"> 
                        <i class="pe-7s-note2"></i> 
                        <p>MATM Support</p>
                    </a>
                </li>';
if($settings['show_conversions']) {
	$sidebar .= '   <li>
       				<a href="conversion-rates.php">
                        <i class="pe-7s-news-paper"></i>
                        <p>Conversion Rates</p>
                    </a>
                </li>';
	$sidebar .= "<li>
       				<a href=\"users.php\">
                        <i class=\"pe-7s-user\"></i>
                        <p>Users</p>
                    </a>
                </li>";
}
$sidebar .= '   <!--<li>
                    <a href="activity_tracking.php">
                        <i class="pe-7s-news-paper"></i>
                        <p>Activity Tracking</p>
                    </a>
                </li>-->
            </ul>
            <div class="logo">
            	<a href="#" class="simple-text">
                	<img src="assets/img/matm-cropped.png"><strong class="logo-img">MATM</strong>
            	</a>
        	</div>
        </div>
    </div>';

function sidebar(){
    global $sidebar;
    print $sidebar;
}

function get_the_sidebar(){
    global $sidebar;
    return $sidebar;
}