type = ['','info','success','warning','danger'];
var mainScroll = 0;

demo = {
    initPickColor: function(){
        $('.pick-class-label').click(function(){
            var new_class = $(this).attr('new-class');  
            var old_class = $('#display-buttons').attr('data-class');
            var display_div = $('#display-buttons');
            if(display_div.length) {
            var display_buttons = display_div.find('.btn');
            display_buttons.removeClass(old_class);
            display_buttons.addClass(new_class);
            display_div.attr('data-class', new_class);
            }
        });
    },
    
    initChartist: function(){    
        
        var dataSales = {
          labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
          series: [
             [287, 385, 490, 492, 554, 586, 698, 695, 752, 788, 846, 944],
            [67, 152, 143, 240, 287, 335, 435, 437, 539, 542, 544, 647],
            [23, 113, 67, 108, 190, 239, 307, 308, 439, 410, 410, 509]
          ]
        };
        
        var optionsSales = {
          lineSmooth: false,
          low: 0,
          high: 800,
          showArea: true,
          height: "245px",
          axisX: {
            showGrid: false,
          },
          lineSmooth: Chartist.Interpolation.simple({
            divisor: 3
          }),
          showLine: false,
          showPoint: false,
        };
        
        var responsiveSales = [
          ['screen and (max-width: 640px)', {
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];
    
        Chartist.Line('#chartHours', dataSales, optionsSales, responsiveSales);
        
    
        var data = {
          labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
          series: [
            [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895],
            [412, 243, 280, 580, 453, 353, 300, 364, 368, 410, 636, 695]
          ]
        };
        
        var options = {
            seriesBarDistance: 10,
            axisX: {
                showGrid: false
            },
            height: "245px"
        };
        
        var responsiveOptions = [
          ['screen and (max-width: 640px)', {
            seriesBarDistance: 5,
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];
        
        Chartist.Bar('#chartActivity', data, options, responsiveOptions);
    
        var dataPreferences = {
            series: [
                [25, 30, 20, 25]
            ]
        };
        
        var optionsPreferences = {
            donut: true,
            donutWidth: 40,
            startAngle: 0,
            total: 100,
            showLabel: false,
            axisX: {
                showGrid: false
            }
        };
    
        Chartist.Pie('#chartPreferences', dataPreferences, optionsPreferences);
        
        Chartist.Pie('#chartPreferences', {
          labels: ['62%','32%','6%'],
          series: [62, 32, 6]
        });   
    },
    
    initGoogleMaps: function(){
        var myLatlng = new google.maps.LatLng(40.748817, -73.985428);
        var mapOptions = {
          zoom: 13,
          center: myLatlng,
          scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
          styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
    
        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        
        var marker = new google.maps.Marker({
            position: myLatlng,
            title:"Hello World!"
        });
        
        // To add the marker to the map, call setMap();
        marker.setMap(map);
    },
    
	showNotification: function(from, align){
    	color = Math.floor((Math.random() * 4) + 1);
    	
    	$.notify({
        	icon: "pe-7s-gift",
        	message: "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."
        	
        },{
            type: type[color],
            timer: 4000,
            placement: {
                from: from,
                align: align
            }
        });
	}

    
}

$(document).ready(function () {
    var output = [];

    $('.Charttable th').click(function () {
        var col = $(this).parent().children().index($(this));
        var table = $(this).closest('table');
        sortTable(table,'asc', col);

    });


    jQuery('.row').each(function () {
        var hieghest = 0;
        $(this).find('> div > div.card').each(function () {
            if($(this).innerHeight() > hieghest){
                hieghest = $(this).innerHeight();
            }
        });
        $(this).find('> div ').each(function () {
            //$(this).height(hieghest)
            $(this).find("> .card").height(hieghest)
        });
    });



   $('.dates-dropdown').daterangepicker({
        timePickerIncrement: 30,
        locale: {
            format: 'DD/MM/YYYY'
        },
    startDate: startdate,
    endDate: enddate
    });

});


$(document).ready(function() {

    LoadStats();
    LoadCharts();
    LoadKeywords();
    
    $(".sortorderForm").submit(function(e){
        e.preventDefault();
        $(".keywords_container").html('<strong class="users saving">Loading<span>.</span><span>.</span><span>.</span></strong>');
        console.log($('.sortorderForm').serialize());
        $.get('keyword-ajax.php?' + $('.sortorderForm').serialize()).success(function (data) {
        	$(".keywords_container").html(data);
        });
    });

    $.get( "ajax.php?type=stats", function(data) {
        $(".key_stats_holder").append(data);
        $(".key_stats_holder .loading").removeClass('loading');
    });

    $(".dates-form").submit(function (e) {
        e.preventDefault();
    });


    var start = moment().subtract(29, 'days');
    var end = moment();
        //$('dates-dropdown input').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

    /*$('.daterangepicker button.applyBtn.btn.btn-sm.btn-success').click(function () {
        $('.dates-form').submit()
    });*/
    var values;
    values = $('.dates-dropdown').val();
    $('.dates-dropdown').change(function () {
        if(values != $('.dates-dropdown').val()){
            setTimeout(formSubmitted(), 1000);
        }
    });

    path = window.location.pathname.substr(window.location.pathname.lastIndexOf('/') + 1);
    $(".sidebar-wrapper a").each(function(link) {
        link = $(this).attr('href');
        if(link == path){
            $(this).addClass("active");
        }
    });

    $('.tableizer-table tr td').hover(
        function () {
            $(this).parent().addClass('hover');
            var index = this.cellIndex + 1;
            $('.tableizer-table tr').each(function (){
                $(this).find(':nth-child(' + index + ')').each(function () {
                    $(this).addClass('hover');
                });
            });
        },
        function () {
            $(this).parent().removeClass('hover');
            var index = this.cellIndex + 1;
            $('.tableizer-table tr').each(function (){
                $(this).find(':nth-child(' + index + ')').each(function () {
                    $(this).removeClass('hover');
                });
            });
        }
    );

    $('.Charttable tr td').hover(
        function () {
            $(this).parent().addClass('hover');
            var index = this.cellIndex + 1;
            $('.tableizer-table tr').each(function (){
                $(this).find(':nth-child(' + index + ')').each(function () {
                    $(this).addClass('hover');
                });
            });
        },
        function () {
            $(this).parent().removeClass('hover');
            var index = this.cellIndex + 1;
            $('.Charttable tr').each(function (){
                $(this).find(':nth-child(' + index + ')').each(function () {
                    $(this).removeClass('hover');
                });
            });
        }
    );

});
var dates;
function formSubmitted() {
    var start, end;
    dates = $('.dates-dropdown').val().replace(/^'|'$/g, "").split(/\s*\-\s*/g);
    start = dates[0].split('/');
    end = dates[1].split('/');
	startd = new Date(start[1] + "/" + start[0] + "/" + start[2]);
	endd = new Date(end[1] + "/" + end[0] + "/" + end[2]);

    first = (endd.getTime() - (endd.getTimezoneOffset() * 60000)) / 1000;
    second = (startd.getTime() - (startd.getTimezoneOffset() * 60000)) / 1000;
    if(first > second){
        endT = first;
        startT = second;
    } else {
        endT = second;
        startT = first;
    }

    window.location = window.location.href.split('?')[0] + "?s=" + startT + "&e=" + endT;
}

function LoadCharts() {
    if(typeof(functions) != "undefined"){
		functions.forEach(function(ThefunctionArray){
        	var TheFunction = ThefunctionArray[0],
            	TheID = ThefunctionArray[1],
	            TheContainerID = ThefunctionArray[2];
    	    ajaxCall(TheFunction, TheID, TheContainerID);
    	});
    }
}

function ajaxCall(TheFunction, TheID, TheContainerID) {
    $.get( "ajax.php?function=" + TheFunction + "&id=" + TheID, function(data) {
        $("." + TheContainerID).append(data);
        $("." + TheContainerID + " .loading").removeClass("loading");
    });
}

function LoadStats() {
    if(typeof(stats) != "undefined"){
    	stats.forEach(function(ThefunctionArray){
        	var TheFunction = ThefunctionArray[0],
            	TheID = ThefunctionArray[1];
	        ajaxCallStats(TheFunction, TheID);
    	});
    }
}

function ajaxCallStats(TheFunction, TheID) {
    $.get( "ajax.php?type=stats&function=" + TheFunction, function(data) {
        $("." + TheID).html(data);
        $("." + TheID + " .loading").removeClass("loading");
    });
}

function switchTable(elem, container, chartdiv){
    var text = elem.innerText;

    if ($(elem).hasClass('table') === true) {
        elem.innerHTML = "Table view";
        console.log($(elem).hasClass('table'));
    }
    if($(elem).hasClass('table') === false){
        elem.innerText = "Chart view";
        console.log($(elem).hasClass('table'));
    }
    $(elem).toggleClass('table');
    $('.' + container + ' .chartTable').toggleClass('hidden');
    $('#' + chartdiv).toggleClass('hidden');

    var chartWidth = $('.' + container + ' .chartTable').width();
    $('.' + container + ' .chartTable tr').first().width(chartWidth);

    return false;
}

function sortTable(table, order, col) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');
        var count = 0;
    tbody.find('tr').not(':first').sort(function(a, b) {
        if (asc) {
            return $('td:nth-child(' + col + ')', a).text().localeCompare($('td:nth-child(' + col + ')', b).text());
        } else {
            return $('td:nth-child(' + col + ')', b).text().localeCompare($('td:nth-child(' + col + ')', a).text());
        }
    }).appendTo(tbody);
}

$('.main-panel').scroll(function (event) {
    mainScroll = $('.main-panel').scrollTop();
    $('.card').not('.heading').each(function () {
        var thehieght = $(this).find('.stickyHeader').height();
        $(this).find('.stickyHeader').css('top', '')
        if($(this).find('.stickyHeader').offset() != undefined) {
            var top = $(this).find('.stickyHeader').offset().top - 50 - mainScroll;
            if(top < 0 - thehieght){
                $(this).find('.stickyHeader').css('top', 0 - thehieght  )
            } else {
                $(this).find('.stickyHeader').css('top', top)
            }
        }
    });
});

function LoadKeywords() {
	if ( $( ".keywords_container" ).length ) {
		$.get("keyword-ajax.php?sortby=ga:adCost&order=-").success(function (data) {
			$( ".keywords_container" ).html(data);
		});
	}
}