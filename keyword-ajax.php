<?php

include_once ('charts.php');
include_once ('database.php');
include_once ('analyticsFunctions.php');
require_once ('ajax-functions.php');
            $counter = -1;
            $total = 1;
            $keywords = get_keywords();
            if ($keywords == false)
                exit;
            if(isset($_GET['sortby'])){
                function aasort (&$array, $key) {
                    $sorter=array();
                    $ret=array();
                    reset($array);
                    foreach ($array as $ii => $va) {
                        $sorter[$ii]=$va[$key];
                    }
                    asort($sorter);
                    foreach ($sorter as $ii => $va) {
                        $ret[$ii]=$array[$ii];
                    }
                    $array=$ret;
                }

                aasort($keywords,$_GET['sortby']);
            }
            print "<div class='row'>";
            foreach (get_keywords_sort(urldecode($_GET['sortby']), urldecode($_GET['order'])) as $keyword => $data){
                if($counter == 2){
                    ?>
                        </div>
                        <div class="row">
                    <?php
                }

                ?>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><?=$keyword?></h4>
                            </div>
                            <div class="content">
                                <p>Total Sessions: <?=$data['users']?></p>
                                <p>Total Conversions: <?=$data['conversions']?></p>
                                <p>Total Cost: £<?=$data['cost']?></p>
                                <p>Total Conversions: <?=$data['conversions']?></p>
                                <p>Total Conversion Rate: <?=round($data['conversionRate'])?>%</p>
                                <p>Total Cost Per Conversion: £<?=$data['costPerConversion']?></p>
                            </div>
                        </div>
                    </div>
                <?php
                if($counter == 2){
                    $counter = 0;
                } elseif($total == count($keywords)) {
                    print "</div>";
                } else {
                    $counter++;
                }
                $total++;
            }

        ?>

                </div>