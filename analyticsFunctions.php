<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 26/09/2017
 * Time: 10:36
 */

if(session_status() != 2){
	session_start();
};

function getStartAndEndDate($week, $year) {
  $dto = new DateTime();
  $ret['week_start'] = $dto->setISODate($year, $week)->format('Y-m-d');
  $ret['week_end'] = $dto->modify('+6 days')->format('Y-m-d');
  return $ret;
}


define('DebugMode', false);
if( !isset($_SESSION['userid'])) {
	print "<script>window.location = 'index.html'</script>";
}
include_once ('GoogleAnalyticsAPI.class.php');
include_once ('charts.php');
include_once ('database.php');
include_once ('analyticsFunctions.php');
//include_once ('conversion-rate-functions.php');

	if(!isset($_GET['s'])){
		if(isset($_SESSION['s'])){
			$_GET['s'] = $_SESSION['s'];
		}
	}
	if(!isset($_GET['e'])){
		if(isset($_SESSION['e'])){
			$_GET['e'] = $_SESSION['e'];
		}
	}

$db = new database();
$ga = new GoogleAnalyticsAPI('service');

$settings = get_settings();

$goalID = $settings["goal"];
?>
	<script>
		var goalid = <?=$goalID?>
	</script>
<?php
$conversionsURL = $settings['conversionsURL'];

if (!file_exists('cache/' . $_SESSION['userid'])) {
	mkdir('cache/' .  $_SESSION['userid'], 0777, true);
}

function  get_settings() {
	global $conversionsURL;
	global $goalID;
	global $db;
	$settings = $db->query("SELECT * FROM settings WHERE user=" . $_SESSION['userid']);
	$array = array();
	foreach ($settings as $setting){
		$key = $setting['option_key'];
		$array[$key] = $setting['value'];
	}
	return $array;
}

function  connect() {
	global $conversionsURL;
	global $goalID;
	global $ga;
	if ( ! isset( $_SESSION['tokenExpires'] ) || ! isset( $_SESSION['tokenCreated'] ) || time() >= ( $_SESSION['tokenCreated'] + $_SESSION['tokenExpires'] ) ) {
		if ( DebugMode == true ) {
			echo "<pre>";
			var_dump( $_SESSION );
			echo "</pre>";
		}
		$ga->auth->setClientId( '786826102984-o69344c8iqoph9c2j4a3defh7g3k722r.apps.googleusercontent.com' ); // From the APIs console
		$ga->auth->setEmail( '786826102984-o69344c8iqoph9c2j4a3defh7g3k722r@developer.gserviceaccount.com' ); // From the APIs console
		$ga->auth->setPrivateKey( 'My Project-67eec1ff0b85.p12' ); // Path to the .p12 file

		$auth = $ga->auth->getAccessToken();
// Try to get the AccessToken
		if ( $auth['http_code'] == 200 ) {
			$_SESSION['accessToken']  = $auth['access_token'];
			$_SESSION['tokenExpires'] = $auth['expires_in'];
			$_SESSION['tokenCreated'] = time();

			return true;
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function  get_visitors(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . $_SESSION['userid'] . "/visitors")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/visitors");
	 } else {
		 fopen("cache/" . $_SESSION['userid'] . "/visitors", 'w');
		 $Cache = "";
    }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/visitors") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['e'])) {
                if (isset($_GET['s'])) {
                    if ($_GET['s'] > $_GET['e']) {
                        print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
                        $_GET['s'] = date('Y-m-d', strtotime('-30 days'));
                        $_GET['e'] = date('Y-m-d');
                    } else {
                       /* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
                            print "	<script type=\"text/javascript\">
                                $(document).ready(function(){
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info',
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
                            $_GET['s'] = strtotime('-30 days');
                            $_GET['e'] = time();
                        }*/
                    }
                }
            }

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );
            $ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
            $params = array(
                'metrics' => 'ga:visits',
                'dimensions' => 'ga:date',
            );
            $visits = $ga->query($params);
            $return = [];
            foreach ($visits['rows'] as $visit) {
	            $date            = $visit[0];
	            $return[ $date ] = $visit[1];
            }
	        $final = [$return, $visits['totalsForAllResults']['ga:visits']];

	        if(!isset($_GET['s']) || !isset($_GET['e'])) {
		        file_put_contents( "cache/" . $_SESSION['userid'] . "/visitors", json_encode( $final ) );
	        }

	        return $final;
        } else {
            return false;
        }
    }
}

function  get_users(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . $_SESSION['userid'] . "/users")) {
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/users");
	 } else {
		 fopen("cache/" . $_SESSION['userid'] . "/users", 'w');
		 $Cache = "";
    }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/users") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );
            $ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
            $params = array(
                'metrics' => 'ga:users'
            );
            $users = $ga->query($params);
	        if(!isset($_GET['s']) || !isset($_GET['e'])) {
		        file_put_contents( "cache/" . $_SESSION['userid'] . "/users", json_encode( $users['rows'][0][0] ) );
	        }
            return $users['rows'][0][0];

        } else {
            return false;
        }
    }
}

function  get_page_visits(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . $_SESSION['userid'] . "/page_visits")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/page_visits");
	} else {
		fopen("cache/" . $_SESSION['userid'] . "/page_visits", 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/page_visits") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id
			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:pageviews'
			);
			$users = $ga->query($params);
			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . $_SESSION['userid'] . "/page_visits", json_encode( $users['rows'][0][0] ) );
			}
			return $users['rows'][0][0];

		} else {
			return false;
		}
	}
}

function  get_session_duration(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . $_SESSION['userid'] . "/session_duration")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/session_duration");
	} else {
		fopen("cache/" . $_SESSION['userid'] . "/session_duration", 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/session_duration") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id
			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:avgSessionDuration'
			);
			$users = $ga->query($params);
			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . $_SESSION['userid'] . "/session_duration", json_encode( gmdate("i:s",$users['rows'][0][0]) ));
			}
			return gmdate("i:s",$users['rows'][0][0]);

		} else {
			return false;
		}
	}
}

function  get_devices(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . $_SESSION['userid'] . "/devices")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/devices");
	} else {
		fopen("cache/" . $_SESSION['userid'] . "/devices", 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/devices") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id
			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:sessions',
				'dimensions' => 'ga:deviceCategory'
			);
			$GAdevices = $ga->query($params);
			$devices = array();
			$count = 0;
			foreach($GAdevices['rows'] as $theDevices) {
				$devices[$theDevices[0]] = $theDevices[1];
				$count += $theDevices[1];
			}
			$devices['all'] = $count;

			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . $_SESSION['userid'] . "/devices", json_encode( $devices ) );
			}
			return $devices;

		} else {
			return false;
		}
	}
}

function  get_avg_Bounces(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . $_SESSION['userid'] . "/avg_Bounces")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/avg_Bounces");
	} else {
		fopen("cache/" . $_SESSION['userid'] . "/avg_Bounces", 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/avg_Bounces") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id
			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:bounceRate',
			);
			$bounce = $ga->query($params);

			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . $_SESSION['userid'] . "/avg_Bounces", json_encode( round($bounce['rows'][0][0], 2) ));
			}
			return round($bounce['rows'][0][0], 2);

		} else {
			return false;
		}
	}
}

	function  get_new_users(){
		global $conversionsURL;
		global $goalID;
		global $ga;
		global $db;
		if(file_exists("cache/" . $_SESSION['userid'] . "/newUsers")) {
			if ( DebugMode == true ) {
				print "Fetching Cache";
			}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/newUsers");
		} else {
			fopen("cache/" . $_SESSION['userid'] . "/newUsers", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/newUsers") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
			return json_decode($Cache, true);
		} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:newUsers'
				);
				$users = $ga->query($params);
				if(!isset($_GET['s']) || !isset($_GET['e'])) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/newUsers", json_encode( $users['rows'][0][0] ) );
				}
				return $users['rows'][0][0];

			} else {
				return false;
			}
		}
	}


function  get_tech(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . $_SESSION['userid'] . "/tech")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
 $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/tech"); 
 } else { 
 fopen("cache/" . $_SESSION['userid'] . "/tech" ,'w');
 $Cache = ""; 
 }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/tech") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id

            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];

            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );
            $ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
            $params = array(
                'metrics' => 'ga:visits',
                'dimensions' => 'ga:browser',
            );
            $browsers = $ga->query($params);

            $return = [];
            foreach ($browsers['rows'] as $browserStat) {
                $browser = $browserStat[0];
                $return[$browser] = $browserStat[1];
            }
	        $final = [$return, $browsers['totalsForAllResults']['ga:visits']];

	        if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . $_SESSION['userid'] . "/tech", json_encode( $final ) );
            }
            return $final;
        } else {
            return false;
        }
    }
}

function  get_conversion_rates(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    global $db;
    if(file_exists("cache/" . $_SESSION['userid'] . "/convRates")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
        $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/convRates"); 
    } else { 
        fopen("cache/" . $_SESSION['userid'] . "/convRates", 'w');
        $Cache = "";
    }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/convRates") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");

        if (connect()) {

            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d',$_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:adCost',
                'dimensions' => 'ga:date',
            );

            $costs = $ga->query($params);


            $costsArray = [];
            foreach ($costs['rows'] as $cost) {
                $Day = $cost[0];
                $costsArray[$Day] = $cost[1];
            }
            
            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal1Completions',
                'dimensions' => 'ga:date',
            );

            $goals = $ga->query($params);
            $goalsArray = [];
            foreach ($goals['rows'] as $goal) {
                $Day = $goal[0];
                $goalsArray[$Day] = $goal[1];
            }


            $cpc = [];
            foreach ($costsArray as $date => $cost) {
                if ($goalsArray[$date] == 0 || $cost == 0) {
                    $cpc[$date] = 0;
                } else {
                    $cpc[$date] = $cost / $goalsArray[$date];
                }
            }
	        $final = [$cpc, $goals['totalsForAllResults']['ga:goal1Completions']];
            if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . $_SESSION['userid'] . "/convRates", json_encode( $final ) );
            }
            return $final;
        } else {
            return false;
        }
    }
}

function  get_conversion_rates_hird(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    global $db;
    if(file_exists("cache/" . $_SESSION['userid'] . "/convRates")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
        $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/convRates"); 
    } else { 
        fopen("cache/" . $_SESSION['userid'] . "/convRates", 'w');
        $Cache = "";
    }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/convRates") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");

        if (connect()) {

            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d',$_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:adCost',
                'dimensions' => 'ga:date',
            );

            $costs = $ga->query($params);


            $costsArray = [];
            foreach ($costs['rows'] as $cost) {
                $Day = $cost[0];
                $costsArray[$Day] = $cost[1];
            }
            
            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal1Completions',
                'dimensions' => 'ga:date',
            );

			$goals = get_conversions();
			
			foreach($goals as $year => $months){
				foreach($months as $month => $days){
					foreach($days as $day => $count){
						$Day = $year . $month . $day;
						$goalsArray[$Day] = $count['count'];
					}
				}
			}

//            $goals = $ga->query($params);
            // $goalsArray = [];
//             foreach ($goals['rows'] as $goal) {
//                 $Day = $goal[0];
//                 var_dump($Day);
//                 $goalsArray[$Day] = $goal[1];
//             }


            $cpc = [];
            foreach ($costsArray as $date => $cost) {
                if ($goalsArray[$date] == 0 || $cost == 0) {
                    $cpc[$date] = 0;
                } else {
                    $cpc[$date] = $cost / $goalsArray[$date];
                }
            }
	        $final = [$cpc, $goals['totalsForAllResults']['ga:goal1Completions']];
            if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . $_SESSION['userid'] . "/convRates", json_encode( $final ) );
            }
            return $final;
        } else {
            return false;
        }
    }
}

function  get_conversion_average(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . $_SESSION['userid'] . "/convAvg")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
        $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/convAvg");
    } else {
        //fopen("cache/" . $_SESSION['userid'] . "/convAvg" 'w');
        $Cache = "";
    }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/convAvg") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return $Cache;
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }

            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:adCost',
            );

            $costs = $ga->query($params);

            $overallCost = $costs['totalsForAllResults']['ga:adCost'];

            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal' . $goalID . 'Completions',
            );
            

            $goals = $ga->query($params);

            $conversions = $goals['totalsForAllResults']['ga:goal' . $goalID . 'Completions'];
            if ($overallCost == 0 || $conversions == 0) {

                if(!isset($_GET['s']) || !isset($_GET['e'])) {
	                file_put_contents( "cache/" . $_SESSION['userid'] . "/convAvg", 0 );
                }

                return 0;
            } else {
                if(!isset($_GET['s']) || !isset($_GET['e'])) {
	                file_put_contents( "cache/" . $_SESSION['userid'] . "/convAvg", $overallCost / $conversions );
                }
                return $overallCost / $conversions;
            }

        }
    }
}


function  get_Percent_conversion_rates(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . $_SESSION['userid'] . "/percentConvRates")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
 $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/percentConvRates"); 
 } else { 
 fopen("cache/" . $_SESSION['userid'] . "/percentConvRates", 'w');
 $Cache = ""; 
 }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/percentConvRates") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Ids
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            /*$params = array(
                'metrics' => 'ga:users',
                'dimensions' => 'ga:date',
            );

            $costs = $ga->query($params);

            $costsArray = [];
            foreach ($costs['rows'] as $cost) {
                $Day = $cost[0];
                $costsArray[$Day] = $cost[1];
            }*/

            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal' . $goalID . 'ConversionRate',
                'dimensions' => 'ga:date',
            );

            $goals = $ga->query($params);
            $goalsArray = [];
            foreach ($goals['rows'] as $goal) {
                $Day = $goal[0];
                $goalsArray[$Day] = $goal[1];
            }
	        $final = [$goalsArray, $goals['totalsForAllResults']['ga:goal' . $goalID . 'ConversionRate']];

	        if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . $_SESSION['userid'] . "/percentConvRates", json_encode( $final ) );
            }
            return $final;
        } else {
            return false;
        }
    }
}

function  get_Percent_conversion_rates_hird(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . $_SESSION['userid'] . "/percentConvRates")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
 $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/percentConvRates"); 
 } else { 
 fopen("cache/" . $_SESSION['userid'] . "/percentConvRates", 'w');
 $Cache = ""; 
 }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/percentConvRates") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Ids
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            /*$params = array(
                'metrics' => 'ga:users',
                'dimensions' => 'ga:date',
            );

            $costs = $ga->query($params);

            $costsArray = [];
            foreach ($costs['rows'] as $cost) {
                $Day = $cost[0];
                $costsArray[$Day] = $cost[1];
            }*/

            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal' . $goalID . 'ConversionRate',
                'dimensions' => 'ga:date',
            );

            $goals = $ga->query($params);
            $goalsArray = [];
            foreach ($goals['rows'] as $goal) {
                $Day = $goal[0];
                $goalsArray[$Day] = $goal[1];
            }
	        $final = [$goalsArray, $goals['totalsForAllResults']['ga:goal' . $goalID . 'ConversionRate']];

	        if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . $_SESSION['userid'] . "/percentConvRates", json_encode( $final ) );
            }
            return $final;
        } else {
            return false;
        }
    }
}

	function get_clicks()
	{
		global $ga;
		global $db;
		if(file_exists("cache/" . $_SESSION['userid'] . "/clicks")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
            $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/clicks");
        } else {
            fopen("cache/" . $_SESSION['userid'] . "/clicks", 'w');
            $Cache = "";
        }

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/clicks") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
			return json_decode($Cache, true);
		} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:adClicks',
					'dimensions' => 'ga:date',
				);
				$clicks = $ga->query($params);
				$return = [];

				foreach ($clicks['rows'] as $visit) {
					$date = $visit[0];
					$return[$date] = $visit[1];
				}
				$final = [$return, $params['totalsForAllResults']['ga:adClicks']];

				if(!isset($_GET['s']) || !isset($_GET['e'])) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/clicks", json_encode( $final ) );
				}
				return $final;
			} else {
				return false;
			}
		}
	}

	function get_spend()
	{
		global $ga;
		global $db;
		if(file_exists("cache/" . $_SESSION['userid'] . "/spend")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/spend");
        } else {
            fopen("cache/" . $_SESSION['userid'] . "/spend", 'w');
            $Cache = "";
        }

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/spend") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
			return json_decode($Cache, true);
		} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'] ;
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:adCost',
					'dimensions' => 'ga:date',
				);
				$clicks = $ga->query($params);
				$return = [];

				foreach ($clicks['rows'] as $visit) {
					$date = $visit[0];
					$return[$date] = $visit[1];
				}
				$final = [$return, $clicks['totalsForAllResults']['ga:adCost']];

				if(!isset($_GET['s']) || !isset($_GET['e'])) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/spend", json_encode( $final ) );
				}

				return $final;
			} else {
				return false;
			}
		}
	}

	function get_impressionsChart()
	{
		global $ga;
		global $db;
		if(file_exists("cache/" . $_SESSION['userid'] . "/impressionsChart")) {
			if ( DebugMode == true ) {
				print "Fetching Cache";
			}
			$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/impressionsChart" );
		} else {
			fopen( "cache/" . $_SESSION['userid'] . "/impressionsChart", 'w' );
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/impressionsChart") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:impressions',
					'dimensions' => 'ga:date',
				);
				$clicks = $ga->query($params);
				$return = [];

				foreach ($clicks['rows'] as $visit) {
					$date = $visit[0];
					$return[$date] = $visit[1];
				}
				$final = [$return, $clicks['totalsForAllResults']['ga:impressions']];
				if(!isset($_GET['s']) || !isset($_GET['e'])) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/impressionsChart", json_encode( $final ) );
				}
				return $final;
			} else {
				return false;
			}
		}
	}

	function get_long_email_clicks()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . $_SESSION['userid'] . "/long_email_clicks")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/long_email_clicks");
		} else {
			fopen("cache/" . $_SESSION['userid'] . "/long_email_clicks", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/long_email_clicks") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			print ( DebugMode ? "grabbing" : "" );
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "mailto:" ) !== false ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[2];
							} else {
								$array[ $date ] = $row[2];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "mailto:" ) !== false ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/long_email_clicks", json_encode( $array ) );
					}
					return $array;
				}
			}
		}
	}

	function get_long_email_clicks_adwords()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;
		if(file_exists("cache/" . $_SESSION['userid'] . "/long_email_clicks_adwords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/long_email_clicks_adwords");
		} else {
			fopen("cache/" . $_SESSION['userid'] . "/long_email_clicks_adwords", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/long_email_clicks_adwords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "cpc" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[3];
							} else {
								$array[ $date ] = $row[3];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/long_email_clicks_adwords", json_encode( $array ) );
					}
					return $array;
				}
			}
		}
	}

	function get_long_email_clicks_organic()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . $_SESSION['userid'] . "/long_email_clicks_adwords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/long_email_clicks_adwords");
		} else {
			fopen("cache/" . $_SESSION['userid'] . "/long_email_clicks_adwords", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/long_email_clicks_adwords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "organic" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[3];
							} else {
								$array[ $date ] = $row[3];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/long_email_clicks_organic", json_encode( $array ) );
					}

					return $array;
				}
			}
		}
	}

function get_long_phone_clicks_adwords()
	{
		global $goalID;
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . $_SESSION['userid'] . "/long_phone_clicks_adwords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/long_phone_clicks_adwords");
		} else {
			fopen("cache/" . $_SESSION['userid'] . "/long_phone_clicks_adwords", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/long_phone_clicks_adwords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "cpc" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[3];
							} else {
								$array[ $date ] = $row[3];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/long_phone_clicks_adwords", json_encode( $array ) );
					}

					return $array;
				}
			}
		}
	}

function get_long_phone_clicks_organic()
	{
		global $goalID;
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . $_SESSION['userid'] . "/long_phone_clicks_organic")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/long_phone_clicks_organic");
		} else {
			fopen("cache/" . $_SESSION['userid'] . "/long_phone_clicks_organic", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/long_phone_clicks_organic") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "organic" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[3];
							} else {
								$array[ $date ] = $row[3];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/long_phone_clicks_organic", json_encode( $array ) );
					}

					return $array;
				}
			}
		}
	}

function get_long_conversions_adwords()
	{
		global $goalID;
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . $_SESSION['userid'] . "/long_conversions_adwords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/long_conversions_adwords");
		} else {
			fopen("cache/" . $_SESSION['userid'] . "/long_conversions_adwords", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/long_conversions_adwords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:goal' . $goalID . 'Completions',
						'dimensions' => 'ga:yearmonth, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( $row[1] == "cpc" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[2];
							} else {
								$array[ $date ] = $row[2];
							}
						}
					}

					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( $row[1] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( $row[1] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/long_conversions_adwords", json_encode( $array ) );
					}

					return $array;
				}
			}
		}
	}

	function get_long_conversions_organic()
	{
		global $goalID;
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . $_SESSION['userid'] . "/long_conversions_organic")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/long_conversions_organic");
		} else {
			fopen("cache/" . $_SESSION['userid'] . "/long_conversions_organic", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/long_conversions_organic") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:goal' . $goalID . 'Completions',
						'dimensions' => 'ga:yearmonth, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( $row[1] == "organic" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[2];
							} else {
								$array[ $date ] = $row[2];
							}
						}
					}

					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( $row[1] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( $row[1] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/long_conversions_organic", json_encode( $array ) );
					}
					return $array;
				}
			}
		}
	}


	function get_long_phone_clicks()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . $_SESSION['userid'] . "/long_phone_clicks")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/long_phone_clicks");
		} else {
			fopen("cache/" . $_SESSION['userid'] . "/long_phone_clicks", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/long_phone_clicks") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "tel:" ) !== false ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[2];
							} else {
								$array[ $date ] = $row[2];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/long_phone_clicks", json_encode( $array ) );
					}
					return $array;
				}
			}
		}
	}

	function get_allConversionsChart()
	{
		$emails = get_long_email_clicks();
		$phone = get_long_phone_clicks();
		$forms = array_reverse(get_long_conversions(), true);

		$array = "";
		foreach ($forms as $date => $formsCount) {
			$thedate = substr($date, 2, 2) . "/" . substr($date, 4, 2);
			if(isset($phone[$date])){
				$phoneClicks = $phone[$date];
			} else {
				$phoneClicks = 0;
			}
			if(isset($emails[$date])){
				$emailClicks = $emails[$date];
			} else {
				$emailClicks = 0;
			}
			if($array == ""){
				$array .= "['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			} else {
				$array .= ",['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			}
		}

		return $array;

	}

	function get_allConversionsTable()
	{
		$emails = get_long_email_clicks();
		$phone = get_long_phone_clicks();
		$forms = array_reverse(get_long_conversions(), true);

		$formsTotal = 0;
		$phoneTotal = 0;
		$emailTotal = 0;

		$html = "<div class='Charttable hidden'>
					<table>
						<thead><tr>
							<th>Date</th>
							<th>Forms</th>
							<th>Phone</th>
							<th>Emails</th>
						</tr></thead>";
		$array = "";
		foreach ($forms as $date => $formsCount) {
			$thedate = substr($date, 2, 2) . "/" . substr($date, 4, 2);
			$html .= "<tr>
						<td>" . $thedate . "</td>
						<td>" . $formsCount ."</td>";
			if(isset($phone[$date])){
				$phoneClicks = $phone[$date];
			} else {
				$phoneClicks = 0;
			}
			$html .= "<td>" . $phoneClicks . "</td>";
			if(isset($emails[$date])){
				$emailClicks = $emails[$date];
			} else {
				$emailClicks = 0;
			}
			$html .= "<td>" . $emailClicks . "</td></tr>";
			$formsTotal += $formsCount;
			$phoneTotal += $phoneClicks;
			$emailTotal += $emailClicks;
		}
		$html .= "<tr><td>Total</td><td>" . $formsTotal . "</td><td>" . $phoneTotal . "</td><td>" . $emailTotal . "</td></tr></table>";

		return $html;

	}


	function get_allConversionsChartAdwords()
	{
		$emails = get_long_email_clicks_adwords();
		$phone = get_long_phone_clicks_adwords();
		$forms = get_long_conversions_adwords();

		$array = "";
		foreach ($forms as $date => $formsCount) {
			$thedate = substr($date, 0, 4) . "/" . substr($date, 4, 2);
			if(isset($phone[$date])){
				$phoneClicks = $phone[$date];
			} else {
				$phoneClicks = 0;
			}
			if(isset($emails[$date])){
				$emailClicks = $emails[$date];
			} else {
				$emailClicks = 0;
			}
			if($array == ""){
				$array .= "['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			} else {
				$array .= ",['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			}
		}

		return $array;

	}


	function get_allConversionsTableAdwords()
	{
		$emails = get_long_email_clicks_adwords();
		$phone = get_long_phone_clicks_adwords();
		$forms = array_reverse(get_long_conversions_adwords(), true);

		$formsTotal = 0;
		$phoneTotal = 0;
		$emailTotal = 0;

		$html = "<div class='Charttable hidden'>
					<table>
						<thead><tr>
							<th>Date</th>
							<th>Forms</th>
							<th>Phone</th>
							<th>Emails</th>
						</tr></thead>";
		$array = "";
		foreach ($forms as $date => $formsCount) {
			$thedate = substr($date, 2, 2) . "/" . substr($date, 4, 2);
			$html .= "<tr>
						<td>" . $thedate . "</td>
						<td>" . $formsCount ."</td>";
			if(isset($phone[$date])){
				$phoneClicks = $phone[$date];
			} else {
				$phoneClicks = 0;
			}
			$html .= "<td>" . $phoneClicks . "</td>";
			if(isset($emails[$date])){
				$emailClicks = $emails[$date];
			} else {
				$emailClicks = 0;
			}
			$html .= "<td>" . $emailClicks . "</td></tr>";
			$formsTotal += $formsCount;
			$phoneTotal += $phoneClicks;
			$emailTotal += $emailClicks;
		}
		$html .= "<tr><td>Total</td><td>" . $formsTotal . "</td><td>" . $phoneTotal . "</td><td>" . $emailTotal . "</td></tr></table>";

		return $html;

	}



	function get_allConversionsChartOrganic()
	{
		$emails = get_long_email_clicks_organic();
		$phone = get_long_phone_clicks_organic();
		$forms = get_long_conversions_organic();

		$array = "";
		foreach ($forms as $date => $formsCount) {
			$thedate = substr($date, 0, 4) . "/" . substr($date, 4, 2);
			if(isset($phone[$date])){
				$phoneClicks = $phone[$date];
			} else {
				$phoneClicks = 0;
			}
			if(isset($emails[$date])){
				$emailClicks = $emails[$date];
			} else {
				$emailClicks = 0;
			}
			if($array == ""){
				$array .= "['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			} else {
				$array .= ",['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			}
		}

		return $array;

	}


	function get_allConversionsTableOrganic()
	{
		$emails = get_long_email_clicks_organic();
		$phone = get_long_phone_clicks_organic();
		$forms = array_reverse(get_long_conversions_organic(), true);

		$formsTotal = 0;
		$phoneTotal = 0;
		$emailTotal = 0;

		$html = "<div class='Charttable hidden'>
					<table>
						<thead><tr>
							<th>Date</th>
							<th>Forms</th>
							<th>Phone</th>
							<th>Emails</th>
						</tr></thead>";
		$array = "";
		foreach ($forms as $date => $formsCount) {
			$thedate = substr($date, 2, 2) . "/" . substr($date, 4, 2);
			$html .= "<tr>
						<td>" . $thedate . "</td>
						<td>" . $formsCount ."</td>";
			if(isset($phone[$date])){
				$phoneClicks = $phone[$date];
			} else {
				$phoneClicks = 0;
			}
			$html .= "<td>" . $phoneClicks . "</td>";
			if(isset($emails[$date])){
				$emailClicks = $emails[$date];
			} else {
				$emailClicks = 0;
			}
			$html .= "<td>" . $emailClicks . "</td></tr>";
			$formsTotal += $formsCount;
			$phoneTotal += $phoneClicks;
			$emailTotal += $emailClicks;
		}
		$html .= "<tr><td>Total</td><td>" . $formsTotal . "</td><td>" . $phoneTotal . "</td><td>" . $emailTotal . "</td></tr></table>";

		return $html;

	}


	function get_long_conversions() {
	global $conversionsURL;
		$conversions = json_decode(file_get_contents($conversionsURL . "?type=month&count=1000"), true);
		$array = array();
		foreach ($conversions as $year => $months){
			foreach ($months as $month => $count){
				$date = "20" . $year . $month;
				$array[$date] = $count;
			}
		}
		return $array;
	}

	function get_conversions() {
		global $conversionsURL;
		$conversions = json_decode(file_get_contents($conversionsURL . "?type=json&count=1000"), true);


		return $conversions;
	}
	
	function get_conversions_week() {
		global $conversionsURL;
		$conversions = json_decode(file_get_contents($conversionsURL . "?type=week&count=1000"), true);


		return $conversions;
	}

function  get_Percent_conversion_rates_average(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . $_SESSION['userid'] . "/percentConvRatesavg")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
 $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/percentConvRatesavg"); 
 } else { 
 //fopen("cache/" . $_SESSION['userid'] . "/percentConvRatesavg" 'w'); 
 $Cache = ""; 
 }

    if ($Cache != "" && filemtime("cache/" . $_SESSION['userid'] . "/percentConvRatesavg") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
        return $Cache;
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            $ga->setDefaultQueryParams($defaults);


            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal' . $goalID . 'ConversionRate'
            );

            $goals = $ga->query($params);
            $conversions = $goals['totalsForAllResults']['ga:goal' . $goalID . 'ConversionRate'];
            if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . $_SESSION['userid'] . "/percentConvRatesavg", $conversions );
            }
            return $conversions;
        }
    }
}

function  get_keywords() {
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;

    if(file_exists("cache/" . $_SESSION['userid'] . "/keywords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
 $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/keywords"); 
 } else { 
 //fopen("cache/" . $_SESSION['userid'] . "/keywords" 'w'); 
 $Cache = ""; 
 }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/keywords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                //$start = date('Y-m-d', strtotime('-30 days'));
                $start = date('Y-m-d', strtotime('29th november 2017'));
                //var_dump($start);
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            $ga->setDefaultQueryParams($defaults);


            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:adCost,ga:goal' . $goalID . 'Completions,ga:sessions',
                'dimensions' => 'ga:keyword'

            );
            $keywords = array();
            $keywordCost = $ga->query($params);

            foreach ($keywordCost['rows'] as $row) {
                $keyword[$row[0]]["cost"] = $row[1];
                $keyword[$row[0]]["conversions"] = $row[2];
                $keyword[$row[0]]["users"] = $row[3];
                if ($keyword[$row[0]]["conversions"] != 0 && $keyword[$row[0]]["users"] != 0) {
                    $keyword[$row[0]]["conversionRate"] = $keyword[$row[0]]["conversions"] / $keyword[$row[0]]["users"];
                } else {
                    $keyword[$row[0]]["conversionRate"] = 0;
                }

                if ($keyword[$row[0]]["cost"] != 0 && $keyword[$row[0]]["conversions"] != 0) {
                    $keyword[$row[0]]["costPerConversion"] = $keyword[$row[0]]["cost"] / $keyword[$row[0]]["conversions"];
                } else {
                    $keyword[$row[0]]["costPerConversion"] = $keyword[$row[0]]["cost"];
                }
            }
            if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . $_SESSION['userid'] . "/keywords", json_encode( $keyword ) );
            }
            return $keyword;

        } else {
            return false;
        }
    }
}


function  get_keywords_sort($sort, $order) {
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    
    //var_dump($sort);
	//var_dump($order);
	//var_dump($order.$sort);

    if(file_exists("cache/" . $_SESSION['userid'] . "/keywords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
 $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/keywords"); 
 } else { 
 //fopen("cache/" . $_SESSION['userid'] . "/keywords" 'w'); 
 $Cache = ""; 
 }

    if(1==0 && $Cache != "" && DebugMode != true && filemtime("cache/" . $_SESSION['userid'] . "/keywords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                //$start = date('Y-m-d', strtotime('-30 days'));
                $start = date('Y-m-d', strtotime('29th november 2017'));
                //var_dump($start);
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            $ga->setDefaultQueryParams($defaults);


            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:adCost,ga:goal' . $goalID . 'Completions,ga:sessions',
                'dimensions' => 'ga:keyword'
            );

            $params['sort'] = $order . $sort;
            
            $keywords = array();
            //var_dump($params);
            $keywordCost = $ga->query($params);

            foreach ($keywordCost['rows'] as $row) {
                $keyword[$row[0]]["cost"] = $row[1];
                $keyword[$row[0]]["conversions"] = $row[2];
                $keyword[$row[0]]["users"] = $row[3];
                if ($keyword[$row[0]]["conversions"] != 0 && $keyword[$row[0]]["users"] != 0) {
                    $keyword[$row[0]]["conversionRate"] = $keyword[$row[0]]["conversions"] / $keyword[$row[0]]["users"];
                } else {
                    $keyword[$row[0]]["conversionRate"] = 0;
                }

                if ($keyword[$row[0]]["cost"] != 0 && $keyword[$row[0]]["conversions"] != 0) {
                    $keyword[$row[0]]["costPerConversion"] = $keyword[$row[0]]["cost"] / $keyword[$row[0]]["conversions"];
                } else {
                    $keyword[$row[0]]["costPerConversion"] = $keyword[$row[0]]["cost"];
                }
            }
            if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . $_SESSION['userid'] . "/keywords", json_encode( $keyword ) );
            }
            return $keyword;

        } else {
            return false;
        }
    }
}

/* USERS */

function  get_userAge(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . $_SESSION['userid'] . "/ages")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/ages");
	} else {
		//fopen("cache/" . $_SESSION['userid'] . "/tech" 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/ages") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id

			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];

			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:users',
				'dimensions' => 'ga:userAgeBracket',
			);
			$ages = $ga->query($params);

			$return = [];
			foreach ($ages['rows'] as $ageRange) {

				$age = $ageRange[0];
				$return[$age] = $ageRange[1];
			}

			$finalReturn = [$return, $ages['totalsForAllResults']['ga:users']];

			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . $_SESSION['userid'] . "/ages", json_encode( $finalReturn ) );
			}
			return $finalReturn;
		} else {
			return false;
		}
	}
}

function enquiryPageVists() {
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . $_SESSION['userid'] . "/enquiryPageVists")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/enquiryPageVists");
	} else {
		//fopen("cache/" . $_SESSION['userid'] . "/tech" 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/enquiryPageVists") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
	//if(1 == 0){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id

			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];

			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:users',
				'dimensions' => 'ga:pagePath, ga:date',
			);
			$thepages = $ga->query($params);
			$return = [];
			$counter = [];
			$items = 0;
			while($items < $thepages['totalResults']){
				foreach ($thepages['rows'] as $thepage) {
					$date = $thepage[1];
					if(!isset($return[$date])){
						$return[$date] = 0;
					}
					if(substr($thepage[0], 0, strlen("/online-enquiry")) == "/online-enquiry") {
						$return[$date] += $thepage[2];
					}
				}
				$items += $thepages['itemsPerPage'];
				$params['start-index'] = $items + 1;
				$thepages = $ga->query($params);
			}
			
			$finalReturn = [$return, $thepages['totalsForAllResults']['ga:users']];

			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . $_SESSION['userid'] . "/enquiryPageVists", json_encode( $finalReturn ) );
			}
			return $finalReturn;
		} else {
			return false;
		}
	}
}

function  get_userGender(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . $_SESSION['userid'] . "/gender")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/gender");
	} else {
		//fopen("cache/" . $_SESSION['userid'] . "/tech" 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/gender") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id

			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];

			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:users',
				'dimensions' => 'ga:userGender',
			);
			$Thegender = $ga->query($params);
			$return = [];
			foreach ($Thegender['rows'] as $genderRange) {

				$gender= $genderRange[0];
				$return[$gender] = $genderRange[1];
			}

			$finalReturn = [$return, $Thegender['totalsForAllResults']['ga:users']];

			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . $_SESSION['userid'] . "/gender", json_encode( $finalReturn ) );
			}
			return $finalReturn;
		} else {
			return false;
		}
	}
}

function  get_userAffinity(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . $_SESSION['userid'] . "/Affinity")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/Affinity");
	} else {
		//fopen("cache/" . $_SESSION['userid'] . "/tech" 'w');
		$Cache = "";
	}

	//if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/Affinity") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
	//	return json_decode($Cache, true);
	//} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id

			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];

			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:users',
				'dimensions' => 'ga:interestAffinityCategory',
			);
			$Thegender = $ga->query($params);
			$return = [];
			foreach ($Thegender['rows'] as $genderRange) {

				
				$return[] = [$genderRange[0], $genderRange[1]] ;
			}


			$finalReturn = [$return, $Thegender['totalsForAllResults']['ga:users']];

			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . $_SESSION['userid'] . "/Affinity", json_encode( $finalReturn ) );
			}
			return $finalReturn;
		} else {
			return false;
	//	}
	}
}



function  get_followthrough_rate(){
		global $ga;
		global $db;
		if(file_exists("cache/" . $_SESSION['userid'] . "/clicks")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
            $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/clicks");
        } else {
            fopen("cache/" . $_SESSION['userid'] . "/clicks", 'w');
            $Cache = "";
        }

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/clicks") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
			return json_decode($Cache, true);
		} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}
				
				if (isset($_SESSION['s'])) {
					$start = date('Y-m-d', $_SESSION['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_SESSION['e'])) {
					$end = date('Y-m-d', $_SESSION['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:goal2Completions',
					'dimensions' => 'ga:isoYearIsoWeek',
				);
				$clicks = $ga->query($params);
				
				$convs = get_conversions_week();
				
				$return = [];
				$total = 0;
				foreach ($clicks['rows'] as $visit) {
					
					$baseYear = substr($visit[0], 0, 4);
					$baseWeek = substr($visit[0], 4, 2);
					foreach($convs as $year => $weeks){
						foreach($weeks as $week => $count){
							if($year == $baseYear && $week == $baseWeek){
								$dates = getStartAndEndDate($week, $year);
								$start = $dates['week_start'];
								$end = $dates['week_end'];
								$string = $start . " - " . $end;
								$return[$string] = round(($count / $visit[1]) * 100, 2);
								$total += $count;
							}
						}
					}
				}
				
				$final = [$return, round(($params['totalsForAllResults']['ga:goal2Completions'])*100, 2)];

				if(!isset($_GET['s']) || !isset($_GET['e'])) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/clicks", json_encode( $final ) );
				}
				return $final;
			} else {
				return false;
			}
		}
	}
	
	function  get_weekly_convs(){
		global $ga;
		global $db;
		if(file_exists("cache/" . $_SESSION['userid'] . "/none")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
            $Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/none");
        } else {
           //fopen("cache/" . $_SESSION['userid'] . "/none", 'w');
            $Cache = "";
        }

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/none") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
			return json_decode($Cache, true);
		} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}
				
				if (isset($_SESSION['s'])) {
					$start = $_SESSION['s'];
				} else {
					$start = strtotime('-30 days');
				}

				if (isset($_SESSION['e'])) {
					$end = $_SESSION['e'];
				} else {
					$end = time();
				}
				
				$convs = get_conversions_week();
				
				$return = [];
				$total = 0;
				
				foreach($convs as $year => $weeks){
					foreach($weeks as $week => $count){
						$time = strtotime($year . 'W' . $week);
						if($time >= $start && $time <= $end){
							$dates = getStartAndEndDate($week, $year);
							$startstr = $dates['week_start'];
							$endstr = $dates['week_end'];
							$string = $startstr . " - " . $endstr;
							$return[$string] = $count;
							$total += $count;
						}
					}
				}
				
				
				$final = [$return, $count];

				if(!isset($_GET['s']) || !isset($_GET['e'])) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/none", json_encode( $final ) );
				}
				return $final;
			} else {
				return false;
			}
		}
	}