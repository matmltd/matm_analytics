<?php

include_once ('functions.php');

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?=$settings['title']?>  Analytics | MATM</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>


    <?=custom_headers()?>

</head>
<body>

<?php sidebar();?>

    <div class="main-panel">
        <?php nav("Keywords"); ?>

<?php get_keywords() ?>

        <div class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="card" style="padding: 10px; text-align: center;">
                        <h2>Keywords</h2>
                        <p><strong>This is a list of keywords people have used to find you. This includes Adwords keywords as well as Organic searches</strong></p>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
	            <form action="keywords-ajax.php" method="GET" class="sortorderForm">
				    <div class="form-group">
				      <label for="sortby">Sort By</label>
				      <select class="form-control" id="sortby" name="sortby">
				      		<option value="ga:adCost">Adwords Cost</option>
				      		<option value="ga:goal<?=$goalID?>Completions">Conversions</option>
				      		<option value="ga:sessions">Sessions</option>
				      </select>
				    </div>
				    <div class="form-group">
				      <label for="order">Order</label>
				      <select class="form-control" id="order" name="order">
				      		<option value="-">Descending</option>
				      		<option value="">Ascending</option>
				      </select>
				    </div>
				  <button type="submit" class="btn btn-primary">Submit</button>
				</form>
    
        		<div class="keywords_container">
	        		<strong class="users saving">Loading<span>.</span><span>.</span><span>.</span></strong>
        		</div>
            </div>
        </div>


        <?php footer()?>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

</html>