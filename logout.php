<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 26/09/2017
 * Time: 14:25
 */
if(session_status() != 2){
	session_start();
};
unset($_SESSION['loggedin']);
unset($_SESSION['token']);
unset($_SESSION['userid']);

header('Location: index.html');