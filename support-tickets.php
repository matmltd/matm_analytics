<?php

include_once ('functions.php');

$settings = get_settings();

$clientSecret = $settings['support_secret'];
function get_tickets() {
    global $clientSecret;
    $url = "http://support.matm.co.uk/tickets.php";

    $tickets = file_get_contents($url . "?clientsecret="  . urlencode($clientSecret) . "&clientid=" . CLIENT_ID);;

    if($tickets != "unauthorised") {
	    return json_decode($tickets, true);
    } else {
        print "There has been an error. Please try again later";
        exit;
    }
}

function get_ticket($id) {
    global $clientSecret;
    $url = "http://support.matm.co.uk/tickets.php";

    $tickets = json_decode(file_get_contents($url . "?clientsecret="  . urlencode($clientSecret) . "&clientid=" . CLIENT_ID . "&ticketid=" . $id), true);

    return $tickets;
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Light Bootstrap Dashboard by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>


    <?=custom_headers()?>

</head>
<body>

<?php sidebar();?>

<div class="main-panel">
    <?php nav("Support"); ?>

    <?php get_keywords();
    if(!isset($_GET['id'])){
    ?>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                        <?php $tickets = get_tickets();
                        if(count($tickets) == 0){
                            ?>
                           <div class="card" style="padding: 10px">
                               <h2>No Open Tickets</h2>
                               <p>You currently have no open support tickets.</p>
                               <p><strong>If you have an issue or request, you can <a href="support.php">raise a new ticket here</a></strong></p>
                            </div>
                            <?php
                        } else {
	                        foreach ( $tickets as $ticket ) {
		                        ?>
                                <div class="card" style="padding: 10px">
                                    <div class="ticket">
                                        <h3>
                                            <a href="support-tickets.php?id=<?= $ticket['id'] ?>"><?= $ticket['subject'] ?></a>
                                        </h3>
                                    </div>
                                </div>
		                        <?php
	                        }
                        }
                        ?>
                </div>
            </div>

        </div>
    </div>
    <?php } else { ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="padding: 10px">
                            <?php $tickets = get_ticket($_GET['id']);
                            foreach ($tickets as $ticket) {
                                ?>
                                <div class="ticket">
                                    <h3><?=$ticket['subject']?></h3>
                                    <p><?=$ticket['ticketBody']?></p>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    <?php } ?>
</div>


<?php footer()?>

</div>
</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

</html>