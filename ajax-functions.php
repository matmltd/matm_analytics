<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 26/04/2018
	 * Time: 11:25
	 */
	function get_page_speed() {
		global $settings;

		if(file_exists("cache/" . $_SESSION['userid'] . "/page_speed")) {
			if ( DebugMode == true ) {
				print "Fetching Cache";
			}
			$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/page_speed");
		} else {
			fopen("cache/" . $_SESSION['userid'] . "/page_speed", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/devices") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
			return json_decode($Cache, true);
		} else {
			print ( DebugMode ? "grabbing" : "" );
			
			$pagespeed      = json_decode( file_get_contents( "https://www.googleapis.com/pagespeedonline/v4/runPagespeed?url=" . urlencode( $settings['url'] ) . "&key=AIzaSyCwRb-1W3FueVrbKO6v5LteQEukKgnaQcw" ) );
			$pagespeedScore = $pagespeed->ruleGroups->SPEED->score;

			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . $_SESSION['userid'] . "/page_speed", json_encode( $pagespeedScore ) );
			}

			return $pagespeedScore;
		}
	}



	function get_adwords_cost() {
		global $ga;
		global $goalID;
		global $db;
		global $db;
		global $requestCache;
		if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost" ) ) {
			if ( DebugMode == true ) {
				print "Fetching Cache";
			}
			$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost" );
		} else {
			fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost", 'w' );
			$Cache = "";
		}

		if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					if ( isset( $_GET['s'] ) ) {
						$start = date( 'Y-m-d', $_GET['s'] );
					} else {
						$start = date( 'Y-m-d', strtotime( '-1 month' ) );
					}

					if ( isset( $_GET['e'] ) ) {
						$end = date( 'Y-m-d', $_GET['e'] );
					} else {
						$end = date( 'Y-m-d' );
					}


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics' => 'ga:adCost',
					);

					$costs = $ga->query( $params );
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost", json_encode( $costs['totalsForAllResults']['ga:adCost'] ) );
					}
					return round($costs['totalsForAllResults']['ga:adCost'], 2);
				}
			}
		}
	}

	function get_sessions() {

		$sessions = 0;
		foreach ( get_visitors()[0] as $date ) {
			$sessions += $date;
		}

		return $sessions;
	}

	function avg_pages_session() {
		return round(get_page_visits()[0]/get_sessions(), 2);
	}

	function get_mobile_sessions() {
		$devices = get_devices();
		return round((($devices['mobile']+$devices['tablet'])/$devices['all']) * 100, 0);
	}