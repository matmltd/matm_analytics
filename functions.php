<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 26/09/2017
 * Time: 10:15
 */
if(session_status() != 2){
	session_start();
}
include_once ('sidebar.php');
include_once ('navigation.php');
include_once ('footer.php');
include_once ('charts.php');
include_once ('database.php');
include_once ('analyticsFunctions.php');
$settings = get_settings();
define('CLIENT_ID', $settings['clientID']);
if(isset($_GET['s'])){
	$_SESSION['s'] = $_GET['s'];
}

if(isset($_GET['e'])){
	$_SESSION['e'] = $_GET['e'];
}

$db = new database();
$siteurl = "http://analytics.matm.co.uk";
//auth();
	if (!file_exists('cache/' . $_SESSION['userid'] . '/conversionsRates')) {
		mkdir('cache/' .  $_SESSION['userid'] . '/conversionsRates', 0777, true);
	}
function get_site_url(){
	global $siteurl;
	return $siteurl;
}
if(!function_exists('get_adwords_cost')){
	function get_adwords_cost() {
		global $ga;
		global $goalID;
		global $db;
		global $db;
		global $requestCache;
		if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost" ) ) {
			if ( DebugMode == true ) {
				print "Fetching Cache";
			}
			$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost" );
		} else {
			fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost", 'w' );
			$Cache = "";
		}

		if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					if ( isset( $_GET['s'] ) ) {
						$start = date( 'Y-m-d', $_GET['s'] );
					} else {
						$start = date( 'Y-m-d', strtotime( '-1 month' ) );
					}

					if ( isset( $_GET['e'] ) ) {
						$end = date( 'Y-m-d', $_GET['e'] );
					} else {
						$end = date( 'Y-m-d' );
					}


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics' => 'ga:adCost',
					);

					$costs = $ga->query( $params );
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost", json_encode( $costs['totalsForAllResults']['ga:adCost'] ) );
					}
					return $costs['totalsForAllResults']['ga:adCost'];
				}
			}
		}
	}
	}

function get_page_speed() {
	global $settings;

	if(file_exists("cache/" . $_SESSION['userid'] . "/page_speed")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/page_speed");
	} else {
		fopen("cache/" . $_SESSION['userid'] . "/page_speed", 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . $_SESSION['userid'] . "/devices") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print ( DebugMode ? "grabbing" : "" );

		$pagespeed      = json_decode( file_get_contents( "https://www.googleapis.com/pagespeedonline/v4/runPagespeed?url=" . urlencode( $settings['url'] ) . "&key=AIzaSyCwRb-1W3FueVrbKO6v5LteQEukKgnaQcw" ) );
		$pagespeedScore = $pagespeed->ruleGroups->SPEED->score;

		if(!isset($_GET['s']) || !isset($_GET['e'])) {
			file_put_contents( "cache/" . $_SESSION['userid'] . "/page_speed", json_encode( $pagespeedScore ) );
		}

		return $pagespeedScore;
	}
}

function auth(){
    global $db;
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
        $result = $db->get_token($_SESSION['userid']);
        if(isset($result[0]) && isset($result[0]['token'])){
            if($_SESSION['token'] === $result[0]['token']){
                return true;
            } else {
                header('Location: index.php');
            }
        } else {
            header('Location: index.php');
        }
    } else {
        header('Location: index.php');
    }
}

function custom_headers() {
    $headers="";


    $headers .= "<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>";
    $headers .= '<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />';
    return $headers;
}

function get_times() {
    if (isset($_GET['s'])) {
        $start = date('d/m/Y', $_GET['s']);
    } else {
        $start = date('d/m/Y', strtotime('-30 days'));
    }

    if (isset($_GET['e'])) {
        $end = date('d/m/Y', $_GET['e']);
    } else {
        $end = date('d/m/Y');
    }
    return $start . " - " .  $end;
}