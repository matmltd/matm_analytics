<?php

	include_once ('functions.php');
    $settings = get_settings();
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?=$settings['title']?> Analytics | MATM</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />


	<!-- Bootstrap core CSS     -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />

	<!-- Animation library for notifications   -->
	<link href="assets/css/animate.min.css" rel="stylesheet"/>

	<!--  Light Bootstrap Table core CSS    -->
	<link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="assets/css/demo.css" rel="stylesheet" />


	<!--     Fonts and icons     -->
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
	<link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
	<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>


	<?=custom_headers()?>

</head>
<body>

<?php sidebar();?>

<div class="main-panel">
	<?php nav("Support"); ?>

	<?php get_keywords();

	if(isset($_POST['clientID'])) {

		$db = new database();
		$sql = "INSERT INTO activities (type, activity, date, client_id) VALUES ('" . $_POST['type'] ."', '" . $_POST['activity'] . "', '" . $_POST['date'] . "', '" . $_POST['clientID'] . "')";
		$db->query_insert($sql);
	}
	$settings = get_settings();
	?>
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card" style="padding: 10px">
						<h2>Log actions on <?=$settings['title']?></h2>
						<div class="row">
							<div class="col-md-6">
								<form action="?" method="POST" class="activity-form">
									<div class="form-group">
										<label for="type">Support Type:</label>
										<select class="form-control" name="type" id="type">
											<option value="On Site">On Site</option>
											<option value="Off Site">Off Site</option>
										</select>
									</div>
									<div class="form-group">
										<label for="activity">Activity:</label>
										<textarea class="form-control" name="activity" id="activity"></textarea>
									</div>
									<div class="form-group">
										<label for="date">Date:</label>
										<input type="date" class="form-control" id="date" name="date">
									</div>
									<input type="hidden" name="clientID" value="<?=CLIENT_ID?>">
									<input type="submit" class="btn btn-default" value="Submit">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div
            <div class="container-fluid">
            <?php
	            if(isset($_GET['all'])){
	            	if (isset($_GET['s'])) {
			            $start = date('Y-m-d', $_GET['s']);
	    	        } else {
		    	        $start = date('Y-m-d', strtotime('-30 days'));
	            	}

		            if (isset($_GET['e'])) {
			            $end = date('Y-m-d', $_GET['e']);
	    	        } else {
		    	        $end = date('Y-m-d');
	            	}

	                $activities = $db->query("SELECT * FROM activities ORDER BY date DESC");
    	            $box = 0;
        	        if(count($activities) > 0){
	        	        foreach($activities as $activity) {
		
    		                if($box == 0) {
	    		                ?>
            		            <div class="row">
	            		        <?php } ?>
                    		<div class="col-md-6">
	                        	<div class="card" style="padding: 10px">
	    	                        <h2><?=$activity['type']?> - <?=$activity['date']?></h2>
    	    	                    <p><?=$activity['activity']?></p>
        	    	            </div>
            	    	    </div>
                	    	    <?php
                    	    	    if($box == 1) {
                        	    	    print "</div>";
                            	    	$box = 0;
	                            	} else {
    	                            	$box = 1;
	        	                    }
    	        	            ?>
		
    		            <?php
        		        }
        		    }
	            } else {
		            if (isset($_GET['s'])) {
			            $start = date('Y-m-d', $_GET['s']);
	    	        } else {
		    	        $start = date('Y-m-d', strtotime('-30 days'));
	            	}

		            if (isset($_GET['e'])) {
			            $end = date('Y-m-d', $_GET['e']);
	    	        } else {
		    	        $end = date('Y-m-d');
	            	}

	                $activities = $db->query("SELECT * FROM activities WHERE date >= '" . $start . "' AND date <= '" . $end . "'  ORDER BY date DESC");
    	            $box = 0;
        	        if(count($activities) > 0){
	        	        foreach($activities as $activity) {
		
    		                if($box == 0) {
	    		                ?>
            		            <div class="row">
	            		        <?php } ?>
                    		<div class="col-md-6">
	                        	<div class="card" style="padding: 10px">
	    	                        <h2><?=$activity['type']?> - <?=$activity['date']?></h2>
    	    	                    <p><?=$activity['activity']?></p>
        	    	            </div>
            	    	    </div>
                	    	    <?php
                    	    	    if($box == 1) {
                        	    	    print "</div>";
                            	    	$box = 0;
	                            	} else {
    	                            	$box = 1;
	        	                    }
    	        	            ?>
		
    		            <?php
        		        }
        	    	} else {
        	    		print "<p>No activities found between " . $start . " and " . $end . ". <a href='?all'>Click Here</a> to see All activites</p>";
	        	    }
        	    }
            ?>
            </div>

        </div>
	</div>
</div>


<?php footer()?>

</div>
</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

</html>