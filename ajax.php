<?php
	include_once ('charts.php');
	include_once ('database.php');
	include_once ('analyticsFunctions.php');
	require_once ('ajax-functions.php');
	if(!isset($_GET['type']) || $_GET['type'] == "charts") {
		$chartFunction = $_GET['function'];
		$chartID       = $_GET['id'];
		print $chartFunction( $chartID );
		print "<script>$('.Charttable tr td').hover(
        function () {
            $(this).parent().addClass('hover');
            var index = this.cellIndex + 1;
            $('.tableizer-table tr').each(function (){
                $(this).find(':nth-child(' + index + ')').each(function () {
                    $(this).addClass('hover');
                });
            });
        },
        function () {
            $(this).parent().removeClass('hover');
            var index = this.cellIndex + 1;
            $('.Charttable tr').each(function (){
                $(this).find(':nth-child(' + index + ')').each(function () {
                    $(this).removeClass('hover');
                });
            });
        }
    );
    jQuery('.row').each(function () {
        var hieghest = 0;
        $(this).find('> div > div.card').each(function () {
            if($(this).innerHeight() > hieghest){
                hieghest = $(this).innerHeight();
            }
        });
        $(this).find('> div ').each(function () {
            //$(this).height(hieghest)
            $(this).find(\"> .card\").height(hieghest)
        });
    });
    loaded = loaded + 1;
    $('#" . $chartID ."').parent().find('.Charttable').prepend('<div class=\"stickyHeader hidden\"><table class=\"\"><thead>' + $('#" . $chartID ."').parent().find('.Charttable tr:first').html() + '</thead></table>');
    $('.card').not('.heading').scroll(function(){
		var offset = ($(this).find('.Charttable').offset().top + $(this).scrollTop()) - $(this).offset().top + 20,
			scroll = $(this).scrollTop(),
			thewidth = $(this).find('.Charttable > table').width();
		if(scroll >= offset){
			$(this).find('.stickyHeader').removeClass('hidden')
			
			$(this).find('.stickyHeader').css('width',thewidth+2)
			
			$(this).find('.stickyHeader').css('top', '')
			var top = $(this).find('.stickyHeader').offset().top - 50 - mainScroll;
			
			$(this).find('.stickyHeader').css('top', top)	
        } else {
			$(this).find('.stickyHeader').addClass('hidden')
        }
        thecounter = 0;
		container = this;
        $(this).find('.Charttable > table th').each(function(header) {
            headerwidth = $(this).width();
            $(container).find('.stickyHeader th').eq(header).width(headerwidth);
			thecounter++; 
        });
	});
    </script>";

	} elseif($_GET['type'] == 'stats') {
		$function = $_GET['function'];
		print $function();
	}