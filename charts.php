<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 26/09/2017
 * Time: 10:34
 */

function trafficChart ($chartID) {

    $theVisitors = get_visitors();

	$visitors = $theVisitors[0];
	$total = $theVisitors[1];

	$html = "";

    if(!empty($visitors)) {

	    $html .= "<div class='Charttable hidden traffic'>
					<table>
						<tr>
							<th>Date</th>
							<th>Users</th>
							<th>Percentage</th>
						</tr>";
        $array = "";
        foreach ($visitors as $date => $visits){

	        $html .= "<tr>
						<td>" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "</td>
						<td>" . $visits ."</td>
						<td>" . round(($visits/$total) * 100, 2) . "%</td>
					  </tr>";
            $array .= ",['" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "'," . $visits . "]";
        }
	    $html .= "<tr><td>Total</td><td>" . $total . "</td><td>" . round(($total/$total) * 100, 2) . "%</td></tr>";

        $html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(trafficChart);

      function trafficChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Visitors']
          " . $array . "
        ]);

        var options = {
          title: 'Visitors',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }
}

function techChart ($chartID){

    $theBrowsers = get_tech();
	$browsers = $theBrowsers[0];
	$total = $theBrowsers[1];
    arsort($browsers);
    if(!empty($browsers)) {
        $array = "";
	    $html = "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Browser</th>
							<th>Users</th>
							<th>Percentage</th>
						</tr>";
        foreach ($browsers as $browser => $visits){
            //if()
            $array .= ",['" .$browser . "'," . $visits . "]";
            $html .= "<tr>
						<td>" . $browser . "</td>
						<td>" . $visits ."</td>
						<td>" . round(($visits/$total) * 100, 2) . "%</td>
					  </tr>";
        }
	    $html .= "<tr><td>Total</td><td>" . $total . "</td><td>" . round(($total/$total) * 100, 2) . "%</td></tr>";

	    $html .= "</table>";

        $html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(techChart);

      function techChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Visitors']
          " . $array . "
        ]);

        var options = {
          title: 'Browsers',
          is3D: true,
          sliceVisibilityThreshold: 1/50
        };

        var chart = new google.visualization.PieChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }
}

function costPerConversionChart($chartID){
    $theCosts = get_conversion_rates();

	$costs = $theCosts[0];
	$total = 0;
	//arsort($browsers);
	$counting = 0;
	$html = "";
    if(!empty($costs)) {
	    $html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Date</th>
							<th>Cost Per Conversion</th>
						</tr>";
	    $array = "";
        foreach ($costs as $date => $cost){
	        $counting++;
	        $total += $cost;
	        $html .= "<tr>
						<td>" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "</td>
						<td>£" . round($cost, 2) ."</td>
					  </tr>";
            $array .= ",['" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "'," . $cost . "]";
        }
	    $html .= "<tr><td>Average</td><td>£" . round($total/$counting, 2) . "</td></tr>";

        $html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(costPerConversionChart);

      function costPerConversionChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Cost']
          " . $array . "
        ]);

        var options = {
          title: 'Cost Per Conversion - Overall Average: £" . round($total/$counting, 2) . "',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }

}

function costPerConversionChartHird($chartID){
    $theCosts = get_conversion_rates_hird();

	$costs = $theCosts[0];
	$total = 0;
	//arsort($browsers);
	$counting = 0;
	$html = "";
    if(!empty($costs)) {
	    $html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Date</th>
							<th>Cost Per Conversion</th>
						</tr>";
	    $array = "";
        foreach ($costs as $date => $cost){
	        $counting++;
	        $total += $cost;
	        $html .= "<tr>
						<td>" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "</td>
						<td>£" . round($cost, 2) ."</td>
					  </tr>";
            $array .= ",['" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "'," . $cost . "]";
        }
	    $html .= "<tr><td>Average</td><td>£" . round($total/$counting, 2) . "</td></tr>";

        $html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(costPerConversionChart);

      function costPerConversionChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Cost']
          " . $array . "
        ]);

        var options = {
          title: 'Cost Per Conversion - Overall Average: £" . round($total/$counting, 2) . "',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }

}

function ClicksChart($chartID) {


	$theClicks = get_clicks();

	$clicks = $theClicks[0];
	$total = $theClicks[1];
	//arsort($browsers);
	$html = "";

	if(!empty($clicks)) {
		$html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Date</th>
							<th>Clicks</th>
						</tr>";
		$array = "";
		$count = 0;
		$clicksTotal = 0;
		foreach ($clicks as $date => $click){
			$html .= "<tr>
						<td>" .  substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "</td>
						<td>" . $click ."</td>
					  </tr>";
			$array .= ",['" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "'," . $click . "]";
			$count++;
			$clicksTotal = $clicksTotal + $click;
		}
		$html .= "<tr><td>Total</td><td>" . $clicksTotal . "</td></tr></table>";

		$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(ClicksChart);

      function ClicksChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Clicks']
          " . $array . "
        ]);

        var options = {
          title: 'Adwords Clicks - Total: " . round($clicksTotal, 2) . " -  Average per day: " . round($clicksTotal / $count, 2) .  "',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Clicks',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
      }
    </script>";

		return $html;
	} else {
		return "";
	}
}

function enquiryPageVistsChart($chartID) {
	


	$theVisits = enquiryPageVists();

	$visits = $theVisits[0];
	$total = $theVisits[1];
	//arsort($browsers);
	$html = "";

	if(!empty($visits)) {
		$html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Date</th>
							<th>Visits</th>
						</tr>";
		$array = "";
		$count = 0;
		$visitsTotal = 0;
		foreach ($visits as $date => $visit){
			$html .= "<tr>
						<td>" .  substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "</td>
						<td>" . $visit ."</td>
					  </tr>";
			$array .= ",['" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "'," . $visit . "]";
			$count++;
			$visitsTotal = $visitsTotal + $visit;
		}
		$html .= "<tr><td>Total</td><td>" . $visitsTotal . "</td></tr></table>";

		$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(ClicksChart);

      function ClicksChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Visits']
          " . $array . "
        ]);

        var options = {
          title: 'Visits to Enquiry Page - Total: " . round($visitsTotal, 2) . " -  Average per day: " . round($visitsTotal / $count, 2) .  "',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Visits',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
      }
    </script>";

		return $html;
	} else {
		return "";
	}

}


function SpendChart($chartID) {


	$theClicks = get_spend();

	$clicks = $theClicks[0];
	$total = $theClicks[1];
	//arsort($browsers);
	$html = "";

	if(!empty($clicks)) {

		$array = "";
		$count = 0;
		$clicksTotal = 0;

		$html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Date</th>
							<th>Spend</th>
							<th>Percentage</th>
						</tr>";

		foreach ($clicks as $date => $click){
			$html .= "<tr>
						<td>" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "</td>
						<td>£" . $click ."</td>
						<td>" . round(($click/$total) * 100, 2) . "%</td>
					  </tr>";
			$array .= ",['" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "'," . $click . "]";
			$count++;
			$clicksTotal += $click;
		}
		$html .= "<tr><td>Total</td><td>" . $total . "</td><td>" . round(($total/$total) * 100, 2) . "%</td></tr></table>";
		$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(SpendChart);

      function SpendChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Cost']
          " . $array . "
        ]);

        var options = {
          title: 'Adwords Spend - Total: £" . round($clicksTotal, 2) . " - Average per day: £" . round($clicksTotal / $count, 2). " ',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Cost (£)',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
      }
    </script>";

		return $html;
	} else {
		return "";
	}
}

function ConversionsChart($chartID) {

	if(file_exists("cache/" . $_SESSION['userid'] . "/ConversionsChart")) {
		$Cache = file_get_contents("cache/" . $_SESSION['userid'] . "/ConversionsChart");
	} else {
		fopen("cache/" . $_SESSION['userid'] . "/ConversionsChart", 'w');
		$Cache = "";
	}
	$Cache = "";

	if ($Cache != "" && filemtime("cache/" . $_SESSION['userid'] . "/ConversionsChart") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
		$array = json_decode($Cache, true);
		$clicksTotal = file_get_contents("cache/" . $_SESSION['userid'] . "/ConversionsChartclicksTotal");
		$count = file_get_contents("cache/" . $_SESSION['userid'] . "/ConversionsChartcount");
		$html = "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Browser</th>
							<th>Users</th>
							<th>Percentage</th>
						</tr>";
	} else {
		print ( DebugMode ? "grabbing" : "" );
		$theConversions = get_conversions();

		if ( ! empty( $theConversions ) ) {

			if ( isset( $_GET['s'] ) ) {
				$start = date( 'Y-m-d', $_GET['s'] );
			} else {
				$start = date( 'Y-m-d', strtotime( '-30 days' ) );
			}

			if ( isset( $_GET['e'] ) ) {
				$end = date( 'Y-m-d', $_GET['e'] );
			} else {
				$end = date( 'Y-m-d' );
			}

			$array       = "";
			$count       = 0;
			$clicksTotal = 0;
			$html = "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Browser</th>
							<th>Users</th>
						</tr>";
			foreach ( $theConversions as $year => $month ) {
				foreach ( $month as $day => $conversions ) {
					foreach ( $conversions as $conversion ) {
						if ( $conversion['time'] >= strtotime( $start ) && $conversion['time'] <= strtotime( $end ) ) {
							$array = ",['" . date( 'd/m/Y', $conversion['time'] ) . "'," . $conversion['count'] . "]" . $array;
							$count ++;
							$htmlArray[] = "<tr>
						<td>" . date( 'd/m/Y', $conversion['time'] ) . "</td>
						<td>" . $conversion['count'] ."</td>
					  </tr>";
							$clicksTotal += $conversion['count'];
						}
					}
				}
			}
			foreach (array_reverse($htmlArray) as $row){
				$html .= $row;
			}
			$html .= "<tr><td>Total</td><td>" . $clicksTotal . "</td></tr></table>";
			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . $_SESSION['userid'] . "/ConversionsChart", json_encode( $array ) );
				file_put_contents( "cache/" . $_SESSION['userid'] . "/ConversionsChartclicksTotal", $clicksTotal );
				file_put_contents( "cache/" . $_SESSION['userid'] . "/ConversionsChartcount", $count );
			}
		}
	}

		$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(ConversionsChart);

      function ConversionsChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Conversions']
          " . $array . "
        ]);

        var options = {
          title: 'Conversions (Forms Submissions) - Total: " . round($clicksTotal, 2) . " - Average per day: " . round($clicksTotal / $count, 2). " ',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Conversions',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
      }
    </script>";

		return $html;
	}

function impressionsChart($chartID) {


	$theImpressions = get_impressionsChart();

	$impressions = $theImpressions[0];
	$total = $theImpressions[1];
	//arsort($browsers);
	$html = "";
	if(!empty($impressions)) {
		$html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Browser</th>
							<th>Impressions</th>
							<th>Percentage</th>
						</tr>";
		$array = "";
		$count = 0;
		$clicksTotal = 0;
		foreach ($impressions as $date => $click){
			$html .= "<tr>
						<td>" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "</td>
						<td>" . $click ."</td>
						<td>" . round(($click/$total) * 100, 2) . "%</td>
					  </tr>";
			$array .= ",['" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "'," . $click . "]";
			$count++;
			$clicksTotal += $click;
		}
		$html .= "<tr><td>Total</td><td>" . $total . "</td><td>" . round(($total/$total) * 100, 2) . "%</td></tr></table>";

		$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(impressionsChart);

      function impressionsChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Impressions']
          " . $array . "
        ]);

        var options = {
          title: 'Impressions - Total: " . round($clicksTotal, 2) . " - Average per day: " . round($clicksTotal / $count, 2). " ',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Impressions',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

		return $html;
	} else {
		return "";
	}
}

function monthlyConversions($chartID) {
	$settings = get_settings();
	$years = json_decode(file_get_contents($settings['conversionsURL'] . "?type=month"));

	$total = 0;
	$array = "";
	foreach ( $years as $year => $months) {
		foreach ($months as $month => $convs){
			$array .= ",['', " . $convs . "]";
			$total += $convs;
		}
	}
	$html = "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Month</th>
							<th>Conversions</th>
							<th>Percentage</th>
						</tr>";
	foreach ( $years as $year => $months) {
		foreach ($months as $month => $convs){
			$html .= "<tr>
						<td>" . $month . "/" . $year . "</td>
						<td>" . $convs ."</td>
						<td>" . round(($convs/$total) * 100, 2) . "%</td>
					  </tr>";
		}
	}
	$html .= "<tr><td>Total</td><td>" . $total . "</td><td>" . round(($total/$total) * 100, 2) . "%</td></tr></table>";

	$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(monthlyconversionsChart);

      function monthlyconversionsChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Enquiries']
          " . $array . "
        ]);

        var options = {
          title: 'Forms filled in per month - Total: " . $total . "',
          curveType: 'function',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'},direction: '-1'},
          vAxis: {title: 'Conversions',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

	return $html;
}

function allConversionsChart($chartID) {
	$impressions = get_allConversionsChart();
	$chart = get_allConversionsTable();
	if ( ! empty( $impressions ) ) {
		$html = $chart;
		$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(allConversionsChart); 

      function allConversionsChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'forms', 'emails', 'phones' ],
          " . $impressions . "
        ]);

        var options = {
          title: 'Conversions by type per month',
          curveType: 'function',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Conversions',minValue: 0},
          colors: ['#559bff', '#b87333', '#e55b46']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

		return $html;
	}
}

function allConversionsChartAdwords($chartID) {
	$impressions = get_allConversionsChartAdwords();
	$chart = get_allConversionsTableAdwords();

	if ( ! empty( $impressions ) ) {

		$html = $chart;
		$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(allConversionsChartAdwords); 

      function allConversionsChartAdwords() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'forms', 'emails', 'phones' ],
          " . $impressions . "
        ]);

        var options = {
          title: 'Conversions by type per month from Adwords',
          curveType: 'function',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Conversions',minValue: 0},
          colors: ['#559bff', '#b87333', '#e55b46']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

		return $html;
	}
}

function allConversionsChartOrganic($chartID) {
	$impressions = get_allConversionsChartOrganic();
	$chart = get_allConversionsTableOrganic();

	if ( ! empty( $impressions ) ) {

		$html = $chart;
		$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(allConversionsChartOrganic); 

      function allConversionsChartOrganic() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'forms', 'emails', 'phones' ],
          " . $impressions . "
        ]);

        var options = {
          title: 'Conversions by type per month from Organic',
          curveType: 'function',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Conversions',minValue: 0},
          colors: ['#559bff', '#b87333', '#e55b46']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

		return $html;
	}
}

function conversionRateChart($chartID){
    $theCosts = get_Percent_conversion_rates();
	$costs = $theCosts[0];
	$total = $theCosts[1];
	//arsort($browsers);
	$html = "";
	$counting = 0;
	$adding = 0;
    if(!empty($costs)) {

	    $html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Date</th>
							<th>Conversion Rate</th>
						</tr>";
        $array = "";
        foreach ($costs as $date => $cost){
	        $counting++;
	        $adding += $cost;
	        $html .= "<tr>
						<td>" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "</td>
						<td>" . round($cost, 2) ."%</td>
					  </tr>";
            $array .= ",['" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "'," . $cost . "]";
        }
	    $html .= "<tr><td>Average</td><td>" . round(($adding/$counting )). "%</td></tr>";

        $html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(conversionRateChart);

      function conversionRateChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'conversion rate (%)']
          " . $array . "
        ]);

        var options = {
          title: 'Average Conversion Rate % - Overall " . round(get_Percent_conversion_rates_average(), 2) . "%',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }

}

function conversionRateChartHird($chartID){
    $theCosts = get_Percent_conversion_rates_hird();
	$costs = $theCosts[0];
	$total = $theCosts[1];
	//arsort($browsers);
	$html = "";
	$counting = 0;
	$adding = 0;
    if(!empty($costs)) {

	    $html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Date</th>
							<th>Conversion Rate</th>
						</tr>";
        $array = "";
        foreach ($costs as $date => $cost){
	        $counting++;
	        $adding += $cost;
	        $html .= "<tr>
						<td>" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "</td>
						<td>" . round($cost, 2) ."%</td>
					  </tr>";
            $array .= ",['" . substr($date, 6, 2) . "/" . substr($date, 4, 2) . "/" .  substr($date, 0, 4) . "'," . $cost . "]";
        }
	    $html .= "<tr><td>Average</td><td>" . round(($adding/$counting )). "%</td></tr>";

        $html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(conversionRateChart);

      function conversionRateChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'conversion rate (%)']
          " . $array . "
        ]);

        var options = {
          title: 'Average Conversion Rate % - Overall " . round(get_Percent_conversion_rates_average(), 2) . "%',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }

}

	function userAgeTable(){
		$Theages = get_userAge();

		$ages = $Theages[0];
		$total = $Theages[1];
		arsort($ages);
		$html = "";
		if(!empty($ages)) {

			$html .= "<div class='Charttable hidden'><table>
						<tr>
							<th>Age Range</th>
							<th>Users</th>
							<th>Percentage</th>
						</tr>";
			foreach ($ages as $age => $count){
				$html .= "<tr>
							<td>" . $age . "</td>
							<td>" . $count. "</td>
							<td>" . round(($count/$total) * 100, 2) . "%</td>
						</tr>";
			}
			$html .= "<tr><td>Total</td><td>" . $total . "</td><td>" . round(($total/$total) * 100, 2) . "%</td></tr>";

			$html .= "</table></div>";
			return $html;
		} else {
			return "";
		}

	}

function userAgeChart($chartID){
    $Theages = get_userAge();

    $ages = $Theages[0];
    $total = $Theages[1];
	arsort($ages);
    if(!empty($ages)) {

        $array = "";
        foreach ($ages as $age => $count){
            $array .= ",['" . $age . "'," . $count . "]";
        }

        $html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(conversionRateChart);

      function conversionRateChart() {
        var data = google.visualization.arrayToDataTable([
          ['Age Range', 'Users']
          " . $array . "
        ]);

        var options = {
          title: 'Age range of Users. Total - " . $total . "',
          hAxis: {title: 'Users',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.BarChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        $html .= userAgeTable();

        return $html;
    } else {
        return "";
    }

}

function userGenderChart($chartID){
    $theGender = get_userGender();

    $genders = $theGender[0];
    $total = $theGender[1];
    arsort($genders);
	$html = "";
    if(!empty($genders)) {

        $array = "";
	    $html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Gender</th>
							<th>Users</th>
							<th>Percentage</th>
						</tr>";
        foreach ($genders as $gender => $count){
        	$html .= "<tr><td>" . $gender . "</td><td>" . $count . "</td><td>" . round(($count/$total) * 100, 2) . "%</td></tr>";
            $array .= ",['" . $gender . "'," . $count . "]";
        }
	    $html .= "<tr><td>Total</td><td>" . $total . "</td><td>" . round(($total/$total) * 100, 2) . "%</td></tr>";

        $html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(conversionRateChart);

      function conversionRateChart() {
        var data = google.visualization.arrayToDataTable([
          ['Gender', 'Users']
          " . $array . "
        ]);

        var options = {
          title: 'Gender of Users. Total - " . $total . "',
          hAxis: {title: 'Users',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.BarChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }

}


function userAffinityChart($chartID){
    $theGender = get_userAffinity();

    $genders = $theGender[0];
    $total = $theGender[1];
	foreach ($genders as $key => $row)
	{
		$realKey = $row[0];
		$theGenders[$realKey] = $row[1];
	}
	arsort($theGenders);
	$html = "";
    if(!empty($theGenders)) {
	    $html .= "<div class='Charttable hidden'><table>
						<tr>
							<th>Age Range</th>
							<th>Users</th>
							<th>Percentage</th>
						</tr>";
        $array = "";
        foreach ($theGenders as $gender => $count) {
	        $html .= "<tr><td>" . $gender . "</td><td>" . $count . "</td><td>" . round(($count/$total) * 100, 2) . "%</td></tr>";
	        $array .= ",['" . str_replace("'", "\'", $gender) . "'," . $count . "]";
        }
	    $html .= "<tr><td>Total</td><td>" . $total . "</td><td>" . round(($total/$total) * 100, 2) . "%</td></tr>";

        $html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(conversionRateChart);

      function conversionRateChart() {
        var data = google.visualization.arrayToDataTable([
          ['Affinity Category', 'Users']
          " . $array . "
        ]);

        var options = {
          title: 'Affinity Category of Users. Total - " . $total . "',
          hAxis: {title: 'Users',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }

}

function followthroughchart($chartID) {

	$theClicks = get_followthrough_rate();

	$clicks = $theClicks[0];
	$total = $theClicks[1];
	//arsort($browsers);
	$html = "";

	if(!empty($clicks)) {
		$html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Date</th>
							<th>Follow through rate</th>
						</tr>";
		$array = "";
		$count = 0;
		$clicksTotal = 0;
		foreach ($clicks as $date => $click){
			$html .= "<tr>
						<td>$date</td>
						<td>" . $click ."</td>
					  </tr>";
			$array .= ",['$date'," . $click . "]";
			$count++;
			$clicksTotal = $clicksTotal + $click;
		}
		$html .= "<tr><td>average</td><td>" . round($clicksTotal / $count, 2) . "</td></tr></table>";

		$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(ClicksChart);

      function ClicksChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Clicks']
          " . $array . "
        ]);

        var options = {
          title: 'Get a qoute form follow through rate -  Average per day: " . round($clicksTotal / $count, 2) .  "',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Follow through rate',minValue: 0},
          colors: ['#ffc20e'],
          series: {
                '1': { pointShape: 'circle' }
            }
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
        
        
        
        
      }
    </script>";

		return $html;
	} else {
		return "";
	}
}


function weekly_convs_chart($chartID) {

	$theClicks = get_weekly_convs();

	$clicks = $theClicks[0];
	$total = $theClicks[1];
	//arsort($browsers);
	$html = "";

	if(!empty($clicks)) {
		$html .= "<div class='Charttable hidden'>
					<table>
						<tr>
							<th>Date</th>
							<th>Follow through rate</th>
						</tr>";
		$array = "";
		$count = 0;
		$clicksTotal = 0;
		foreach ($clicks as $date => $click){
			$html .= "<tr>
						<td>$date</td>
						<td>" . $click ."</td>
					  </tr>";
			$array .= ",['$date'," . $click . "]";
			$count++;
			$clicksTotal = $clicksTotal + $click;
		}
		//$html .= "<tr><td>average</td><td>" . round($clicksTotal / $count, 2) . "</td></tr></table>";

		$html .= "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(ClicksChart);

      function ClicksChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Clicks']
          " . $array . "
        ]);

        var options = {
          title: 'Get a qoute form follow through rate -  Average per day: " . round($clicksTotal / $count, 2) .  "',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Follow through rate',minValue: 0},
          colors: ['#ffc20e'],
          series: {
                '1': { pointShape: 'circle' }
            }
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
        
        
        
        
      }
    </script>";

		return $html;
	} else {
		return "";
	}
}
