<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 29/09/2017
 * Time: 09:27
 */
$footer = '<footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                         
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy; ' . date('Y') . ' <a href="http://www.matm.co.uk">MATM</a>
                </p>
            </div>
        </footer>';

function footer(){
    global $footer;
    print $footer;
}

function get_the_footer(){
    global $footer;
    return $footer;
}