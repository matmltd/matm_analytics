<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 26/09/2017
 * Time: 11:36
 */

class database
{
    public $conn;

    public function __construct()
    {
        $servername = "localhost";
        $username = "matm_support";
        $password = "studio32";
        $dbname = "matm_analytics";

        $this->conn = new mysqli($servername, $username, $password, $dbname);

        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }

	public function query_insert($sql) {
		$result = $this->conn->query($sql);
		if($result != false) {
			return $result;
		} else {
			return $this->conn->error;
		}
	}

    public function query($sql)
    {

        $result = $this->conn->query($sql);

        if (is_object($result) && $result->num_rows > 0) {
            // output data of each row
            $results = array();
            while ($row = $result->fetch_assoc()) {
                array_push($results, $row);
            }
            return $results;
        } else {
            return [];
        }
    }

    public function get_token($id)
    {
        $results = $this->query("SELECT token FROM users WHERE id=$id");
        return $results;
    }

    public function __destruct()
    {
        $this->conn->close();
    }
}