<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 17/10/2017
 * Time: 13:59
 */

include_once ('GoogleAnalyticsAPI.class.php');
$db = new database();
$ga = new GoogleAnalyticsAPI('service');
$requestCache = 0;

	$settings = get_settings();

	$goalID = $settings["goal"];

	$conversionsURL = $settings['conversionsURL'];

	$formPageGoal = $settings['form_page_goal'];
if (!file_exists('cache/' . $_SESSION['userid'] . '/conversionsRates')) {
	mkdir('cache/' .  $_SESSION['userid'] . '/conversionsRates', 0777, true);
}
if(!function_exists('get_adwords_cost')) {
	function get_adwords_cost() {
		global $ga;
		global $goalID;
		global $db;
		global $db;
		global $requestCache;
		if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost" ) ) {
			if ( DebugMode == true ) {
				print "Fetching Cache";
			}
			$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost" );
		} else {
			fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost", 'w' );
			$Cache = "";
		}

		if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
					$ga->setAccountId( $id );

					if ( isset( $_GET['s'] ) ) {
						$start = date( 'Y-m-d', $_GET['s'] );
					} else {
						$start = date( 'Y-m-d', strtotime( '-1 month' ) );
					}

					if ( isset( $_GET['e'] ) ) {
						$end = date( 'Y-m-d', $_GET['e'] );
					} else {
						$end = date( 'Y-m-d' );
					}


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics' => 'ga:adCost',
					);

					$costs = $ga->query( $params );
					if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
						file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_cost", json_encode( $costs['totalsForAllResults']['ga:adCost'] ) );
					}

					return $costs['totalsForAllResults']['ga:adCost'];
				}
			}
		}
	}
}

function get_adwords_clicks() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;
	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_clicks" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_clicks" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_clicks", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_clicks" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics' => 'ga:adClicks',
				);

				$costs = $ga->query( $params );
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/adwords_clicks", json_encode( $costs['totalsForAllResults']['ga:adClicks'] ) );
				}

				return $costs['totalsForAllResults']['ga:adClicks'];
			}
		}
	}
}

function get_GAQ_page_visits() {
	global $ga;
	global $goalID;
	global $db;
	global $db;
	global $requestCache;
	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/GAQ_page_visits" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/GAQ_page_visits" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/GAQ_page_visits", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/GAQ_page_visits" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:goal2Completions',
					'dimensions' => 'ga:source'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;
				foreach ( $costs['rows'] as $row ) {
					if ( $row[0] == "google" ) {
						$clicks += $row[1];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/GAQ_page_visits", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}


function get_form_submissions() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;
	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/form_submissions" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/form_submissions" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/form_submissions", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/form_submissions" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:goal1Completions',
					'dimensions' => 'ga:source'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;
				foreach ( $costs['rows'] as $row ) {
					if ( $row[0] == "google" ) {
						$clicks += $row[1];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/form_submissions", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}

function get_email_clicks() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;

	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/email_clicks" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/email_clicks" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/email_clicks", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/email_clicks" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:totalEvents',
					'dimensions' => 'ga:eventLabel, ga:source'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;
				foreach ( $costs['rows'] as $row ) {
					if ( strpos( $row[0], "mailto:" ) !== false && $row[1] == "google" ) {
						$clicks += $row[2];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/email_clicks", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}

function get_phone_clicks() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;
	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/phone_clicks" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/phone_clicks" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/phone_clicks", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/phone_clicks" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:totalEvents',
					'dimensions' => 'ga:eventLabel, ga:source'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;
				foreach ( $costs['rows'] as $row ) {
					if ( strpos( $row[0], "tel:" ) !== false && $row[1] == "google" ) {
						$clicks += $row[2];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/phone_clicks", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}

function get_fb_clicks() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;

	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_clicks" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_clicks" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_clicks", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_clicks" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:campaign',
					'dimensions' => 'ga:source'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;
				foreach ( $costs['rows'] as $row ) {
					if ( strpos( $row[0], "Facebook_London" ) !== false || strpos( $row[0], "Facebook_Birmingham" ) !== false ) {
						$clicks += $row[1];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_clicks", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}

function get_fb_GAQ_page_visits() {
	global $ga;
	global $formPageGoal;
	global $db;
	global $db;
	global $requestCache;
	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_GAQ_page_visits" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_GAQ_page_visits" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_GAQ_page_visits", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_GAQ_page_visits" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:goal' . $formPageGoal . 'Completions',
					'dimensions' => 'ga:source'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;

				foreach ( $costs['rows'] as $row ) {
					if ( strpos( $row[0], "facebook" ) !== false || strpos( $row[0], "(direct)" ) !== false ) {
						$clicks += $row[1];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_GAQ_page_visits", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}

function get_fb_form_submissions() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;

	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_form_submissions" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_form_submissions" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_form_submissions", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_form_submissions" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:goal1Completions',
					'dimensions' => 'ga:source'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;
				foreach ( $costs['rows'] as $row ) {
					if ( strpos( $row[0], "facebook" ) !== false || strpos( $row[0], "(direct)" ) !== false ) {
						$clicks += $row[1];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_form_submissions", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}

function get_fb_email_clicks() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;

	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_email_clicks" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_email_clicks" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_email_clicks", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_email_clicks" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:totalEvents',
					'dimensions' => 'ga:eventLabel, ga:source'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;
				foreach ( $costs['rows'] as $row ) {
					if ( strpos( $row[0], "mailto:" ) !== false && ( strpos( $row[1], "facebook" ) !== false || strpos( $row[1], "(direct)" ) !== false ) ) {
						$clicks += $row[2];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_email_clicks", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}

function get_fb_phone_clicks() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;
	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_phone_clicks" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_phone_clicks" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_phone_clicks", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_phone_clicks" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:totalEvents',
					'dimensions' => 'ga:eventLabel,ga:source'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;

				foreach ( $costs['rows'] as $row ) {
					if ( strpos( $row[0], "tel:" ) !== false && ( strpos( $row[1], "facebook" ) !== false || strpos( $row[1], "direct" ) !== false ) ) {
						$clicks += $row[2];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/fb_phone_clicks", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}

function get_total_clicks() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;

	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_clicks" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_clicks" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_clicks", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_clicks" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics' => 'ga:sessions',
				);

				$costs = $ga->query( $params );
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_clicks", json_encode( $costs['totalsForAllResults']['ga:sessions'] ) );
				}

				return $costs['totalsForAllResults']['ga:sessions'];
			}
		}
	}
}

function get_total_GAQ_page_visits() {
	global $ga;
	global $formPageGoal;
	global $db;
	global $db;
	global $requestCache;

	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_GAQ_page_visits" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_GAQ_page_visits" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_GAQ_page_visits", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_GAQ_page_visits" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics' => 'ga:goal' . $formPageGoal . 'Completions',
				);

				$costs = $ga->query( $params );
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_GAQ_page_visits", json_encode( $costs['totalsForAllResults']['ga:goal' . $formPageGoal . 'Completions'] ) );
				}

				return $costs['totalsForAllResults']['ga:goal' . $formPageGoal . 'Completions'];
			}
		}
	}
}

function get_total_form_submissions() {
	global $ga;
	global $goalID;
	global $db;
	global $db;
	global $requestCache;
	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_form_submissions" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_form_submissions" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_form_submissions", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_form_submissions" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics' => 'ga:goal1Completions',
				);

				$costs = $ga->query( $params );
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_form_submissions", json_encode( $costs['totalsForAllResults'][ 'ga:goal1Completions' ] ) );
				}

				return $costs['totalsForAllResults'][ 'ga:goal1Completions' ];
			}
		}
	}
}

function get_total_email_clicks() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;
	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_email_clicks" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_email_clicks" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_email_clicks", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_email_clicks" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:totalEvents',
					'dimensions' => 'ga:eventLabel'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;
				foreach ( $costs['rows'] as $row ) {
					if ( strpos( $row[0], "mailto:" ) !== false ) {
						$clicks += $row[1];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_email_clicks", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}

function get_total_phone_clicks() {
	global $ga;
	global $db;
	global $db;
	global $requestCache;
	if ( file_exists( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_phone_clicks" ) ) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_phone_clicks" );
	} else {
		fopen( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_phone_clicks", 'w' );
		$Cache = "";
	}

	if ( $Cache != "" && filemtime( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_phone_clicks" ) > strtotime( "-30 minutes", time() ) && ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) ) {
		return json_decode( $Cache, true );
	} else {
		if ( connect() ) {
			if ( $requestCache == 0 ) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken( $_SESSION['accessToken'] );
				$id = $db->query( "SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'] )[0]['propertyId'];
				$ga->setAccountId( $id );

				if ( isset( $_GET['s'] ) ) {
					$start = date( 'Y-m-d', $_GET['s'] );
				} else {
					$start = date( 'Y-m-d', strtotime( '-1 month' ) );
				}

				if ( isset( $_GET['e'] ) ) {
					$end = date( 'Y-m-d', $_GET['e'] );
				} else {
					$end = date( 'Y-m-d' );
				}


				$defaults = array(
					'start-date' => $start,
					'end-date'   => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams( $defaults );

				$params = array(
					'metrics'    => 'ga:totalEvents',
					'dimensions' => 'ga:eventLabel'
				);

				$costs  = $ga->query( $params );
				$clicks = 0;

				foreach ( $costs['rows'] as $row ) {
					if ( strpos( $row[0], "tel:" ) !== false ) {
						$clicks += $row[1];
					}
				}
				if ( ! isset( $_GET['s'] ) || ! isset( $_GET['e'] ) ) {
					file_put_contents( "cache/" . $_SESSION['userid'] . "/conversionsRates/total_phone_clicks", json_encode( $clicks ) );
				}

				return $clicks;
			}
		}
	}
}
