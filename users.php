<?php

include_once ('functions.php');

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?=$settings['title']?>  Analytics | MATM</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script>
        var functions = [];
        function registerFunction(Thefunction, id, containerID) {
            functions.push([Thefunction, id, containerID]);
        }
        var stats = [];
    </script>

    <?=custom_headers()?>

</head>
<body>

<?php sidebar();?>

    <div class="main-panel">
        <?php nav("Keywords"); ?>

<?php get_keywords() ?>

        <div class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="card" style="padding: 10px; text-align: center;">
                        <h2>Users</h2>
                        <p><strong>This shows data on who is visiting the site.</strong></p>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card first">
                            <div class="table-switch"><a href="#" onclick="return switchTable(this, 'first', 'userAge')">Table view</a></div><div class="loading"></div>
                            <div id="userAge" style="height: 477px;"></div>
                            <script>$(document).load(registerFunction('userAgeChart', 'userAge', 'first'))</script>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card second">
                            <div class="table-switch"><a href="#" onclick="return switchTable(this, 'second', 'userGender')">Table view</a></div><div class="loading"></div>
                            <div id="userGender" style="height: 477px;"></div>
                            <script>$(document).load(registerFunction('userGenderChart', 'userGender', 'second'))</script>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card third">
                            <div class="table-switch"><a href="#" onclick="return switchTable(this, 'third', 'userAffinity')">Table view</a></div><div class="loading"></div>
                            <div id="userAffinity" style="height: 477px;"></div>
                            <script>$(document).load(registerFunction('userAffinityChart', 'userAffinity', 'third'))</script>
                        </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
            </div>
        </div>
    </div>


        <?php footer()?>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

</html>